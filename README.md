## Pipeline status

## command-service
- master: [![pipeline status](https://gitlab.com/advprog-teman-ai-microservices/command-service/badges/master/pipeline.svg)](https://gitlab.com/advprog-teman-ai-microservices/command-service/-/commits/master)
[![coverage report](https://gitlab.com/advprog-teman-ai-microservices/command-service/badges/master/coverage.svg)](https://gitlab.com/advprog-teman-ai-microservices/command-service/-/commits/master)
- alghi:  [![pipeline status](https://gitlab.com/advprog-teman-ai-microservices/command-service/badges/alghi/pipeline.svg)](https://gitlab.com/advprog-teman-ai-microservices/command-service/-/commits/alghi)
[![coverage report](https://gitlab.com/advprog-teman-ai-microservices/command-service/badges/alghi/coverage.svg)](https://gitlab.com/advprog-teman-ai-microservices/command-service/-/commits/alghi)
- rocky-dev:  [![pipeline status](https://gitlab.com/advprog-teman-ai-microservices/command-service/badges/rocky-dev/pipeline.svg)](https://gitlab.com/advprog-teman-ai-microservices/command-service/-/commits/rocky-dev)
[![coverage report](https://gitlab.com/advprog-teman-ai-microservices/command-service/badges/rocky-dev/coverage.svg)](https://gitlab.com/advprog-teman-ai-microservices/command-service/-/commits/rocky-dev)
- lia-fitur:  [![pipeline status](https://gitlab.com/advprog-teman-ai-microservices/command-service/badges/lia-fitur/pipeline.svg)](https://gitlab.com/advprog-teman-ai-microservices/command-service/-/commits/lia-fitur)
[![coverage report](https://gitlab.com/advprog-teman-ai-microservices/command-service/badges/lia-fitur/coverage.svg)](https://gitlab.com/advprog-teman-ai-microservices/command-service/-/commits/lia-fitur)
- inez:  [![pipeline status](https://gitlab.com/advprog-teman-ai-microservices/command-service/badges/inez/pipeline.svg)](https://gitlab.com/advprog-teman-ai-microservices/command-service/-/commits/inez)
[![coverage report](https://gitlab.com/advprog-teman-ai-microservices/command-service/badges/inez/coverage.svg)](https://gitlab.com/advprog-teman-ai-microservices/command-service/-/commits/inez)

## linebot-service
- master: [![pipeline status](https://gitlab.com/advprog-teman-ai-microservices/linebot-service/badges/master/pipeline.svg)](https://gitlab.com/advprog-teman-ai-microservices/linebot-service/-/commits/master)
[![coverage report](https://gitlab.com/advprog-teman-ai-microservices/linebot-service/badges/master/coverage.svg)](https://gitlab.com/advprog-teman-ai-microservices/linebot-service/-/commits/master)

## eureka-service
- master:[![pipeline status](https://gitlab.com/advprog-teman-ai-microservices/eureka-service/badges/master/pipeline.svg)](https://gitlab.com/advprog-teman-ai-microservices/eureka-service/-/commits/master)

## Nama Kelompok:
- Firdaus Al-ghifari (1806133780)
- Rocky Arkan (1806186566)
- Lia Yuliana (1806141271)
- Inez Amandha Suci (1806141233)

## Deskripsi Aplikasi:
Teman.ai (Team Management Artificial Intelligence) adalah sebuah ChatBot berbasis aplikasi Line yang bertujuan untuk memanage task dalam suatu team dengan menggunakan fitur to-do-list dari Trello.

## How to add Teman.ai
Scan QR Code ini di aplikasi Line anda

![](qr-teman-ai.png)
