package ai.teman.commandservice.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "board_room")
public class BoardRoom {
    @Id
    private String id;

    @Column(name = "board_id")
    private String boardId;

    public BoardRoom() {

    }

    public BoardRoom(String id, String boardId) {
        this.id = id;
        this.boardId = boardId;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setBoardId(String boardId) {
        this.boardId = boardId;
    }

    public String getId() {
        return id;
    }

    public String getBoardId() {
        return boardId;
    }
}
