package ai.teman.commandservice.controller;

import ai.teman.commandservice.service.CommandService;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@RestController
public class CommandServiceController {

    private CommandService commandService;
    private RestTemplate restTemplate;

    @Autowired
    public CommandServiceController(
            CommandService commandService, RestTemplate restTemplate
    ){
        this.restTemplate = restTemplate;
        this.commandService = commandService;
        commandService.setController(this);
    }

    @GetMapping("/hello/")
    public String justHello() {
        return "Hello guys";
    }

    @PostMapping("/process/")
    @JsonIgnoreProperties(ignoreUnknown = true)
    public Message processCommand(@RequestBody List<String> jsonList){
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        String message = null;
        Source chatSource = null;
        try {
            message = mapper.readValue(jsonList.get(0), String.class);
            chatSource = mapper.readValue(jsonList.get(1), Source.class);
            return commandService.processCommand(message, chatSource);
        } catch (JsonProcessingException e){
            return new TextMessage("Error ketika memparse json. Kontak developer.");
        }
    }

    public Message sendPushMessage(String senderID, String message) throws JsonProcessingException{
        ObjectMapper mapper = new ObjectMapper();

        //configuring for test
        //kalo didelete, jadi error infinite recursion karena source harus dimock di test
        //references:
        //https://stackoverflow.com/questions/22851462/infinite-recursion-when-serializing-objects-with-jackson-and-mockito
        mapper.setAnnotationIntrospector(new JacksonAnnotationIntrospector() {

            @Override
            public boolean hasIgnoreMarker(final AnnotatedMember m) {
                return super.hasIgnoreMarker(m) || m.getName().contains("Mockito");
            }
        });
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);

        String jsonSenderID = mapper.writeValueAsString(senderID);
        String jsonMessage = mapper.writeValueAsString(message);
        List<String> listJson = new ArrayList<>();
        listJson.add(jsonSenderID);
        listJson.add(jsonMessage);
        return restTemplate.postForObject("http://linebot-service/push-message/",
                new HttpEntity<List<String>>(listJson), Message.class);
    }
}

@Configuration
class RestTemplateConfig {

    // Create a bean for restTemplate to call services
    @Bean
    @LoadBalanced        // Load balance between service instances running at different ports.
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}

