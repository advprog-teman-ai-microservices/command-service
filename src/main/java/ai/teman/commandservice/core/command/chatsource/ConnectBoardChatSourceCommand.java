package ai.teman.commandservice.core.command.chatsource;

import ai.teman.commandservice.core.builder.FlexMessageBuilder;
import ai.teman.commandservice.core.trello.TrelloApi;
import ai.teman.commandservice.core.trello.TrelloBoard;
import ai.teman.commandservice.data.BoardRoom;
import ai.teman.commandservice.repository.BoardRoomRepo;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;

public class ConnectBoardChatSourceCommand implements ChatSourceCommand {

    private BoardRoomRepo boardRoomRepo;

    public ConnectBoardChatSourceCommand(BoardRoomRepo boardRoomRepo) {
        this.boardRoomRepo = boardRoomRepo;
    }

    @Override
    public String getName() {
        return "/connectBoard";
    }

    @Override
    public Message processCommand(String message, Source chatSource) {
        String[] messageLst = message.split(" ");
        if (messageLst.length == 2) {
            try {
                String idOrUrl = messageLst[1];
                if (idOrUrl.contains("/b/")) {
                    // idOrUrl is a URL
                    String[] parsedUrl = idOrUrl.split("/");
                    for (int i = 0; i < parsedUrl.length; ++i)
                        if (parsedUrl[i].equals("b")) {
                            idOrUrl = parsedUrl[i + 1];
                            break;
                        }
                }
                TrelloBoard boardInfo = TrelloApi.getInstance().getBoardInfo(idOrUrl);

                BoardRoom boardRoom = new BoardRoom(chatSource.getSenderId(), boardInfo.id);
                boardRoomRepo.save(boardRoom);

                String msg = "Board " + boardInfo.name + " is successfully connected";
                FlexMessageBuilder builder = new FlexMessageBuilder(msg);
                return builder.createTitleText("Connect Board")
                        .createSeparatorLine()
                        .createBodyText(msg)
                        .buildFlexMessage()
                        .get();
            } catch (Exception e) {
                return new TextMessage("Couldn't connect to board " + messageLst[1] + ".\n" +
                        "Have you invite @temanai to the board?");
            }
        } else {
            return new TextMessage("Invalid argument for /connectBoard. Read /help");
        }
    }

    @Override
    public String getDetail() {
        return "Command untuk menyambungkan chat ke sebuah Trello Board.\n"
                + "How to use:\n/connectBoard <URL atau ID Board>\n"
                + "Contoh : /connectBoard lAkskZAcz";
    }

}
