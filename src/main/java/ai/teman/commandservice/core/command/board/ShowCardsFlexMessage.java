package ai.teman.commandservice.core.command.board;

import ai.teman.commandservice.core.trello.TrelloApi;
import ai.teman.commandservice.core.trello.TrelloCard;
import ai.teman.commandservice.core.trello.TrelloList;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.container.Carousel;

import java.util.*;

public class ShowCardsFlexMessage {
    private List<Bubble> listBubbles = new ArrayList<>();
    private Carousel carousel;

    public FlexMessage get(TrelloList[] trelloLists) {
        List<HashMap<String, List<TrelloCard>>> collection = new ArrayList<>();
        HashMap<String, List<TrelloCard>> listAndCard = new HashMap<>();

        for (TrelloList list : trelloLists){
            TrelloCard[] trelloCards = TrelloApi.getInstance().getListCards(list.id);
            List<TrelloCard> cards  = new ArrayList<>(trelloCards.length);
            Collections.addAll(cards,trelloCards);
            listAndCard.put(list.name, cards);
        }
        collection.add(listAndCard);

        for (HashMap<String, List<TrelloCard>> each : collection) {
            for (Map.Entry<String, List<TrelloCard>> entry : each.entrySet()) {
                if (entry.getValue().isEmpty()) {
                    carousel =
                            Carousel.builder()
                                    .contents(listNotContainsCard(entry.getKey(), "There's no card in this list."))
                                    .build();
                } else {
                    carousel =
                            Carousel.builder()
                                    .contents(listContainsCard(entry.getKey(), entry.getValue()))
                                    .build();
                }
            }
        }
        return new FlexMessage("Show Cards", carousel);
    }

    public List<Bubble> listContainsCard(String listName, List<TrelloCard> listContent) {
        this.listBubbles.add(new InfoBubble()
                .createBubbleContainsCard(listName, listContent));
        return this.listBubbles;
    }

    public List<Bubble> listNotContainsCard (String listName, String text) {
        this.listBubbles.add(new InfoBubble()
                .createBubbleNotContainsCard(listName, text));
        return this.listBubbles;
    }
}