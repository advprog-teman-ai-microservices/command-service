package ai.teman.commandservice.core.command.basic.help;

import ai.teman.commandservice.core.command.basic.BasicFlexMessageCommand;
import ai.teman.commandservice.core.command.basic.help.flex_message.ConnectBoardHelpFlexMessage;
import com.linecorp.bot.model.message.FlexMessage;

public class ConnectBoardHelpCommand implements BasicFlexMessageCommand {
    @Override
    public String getName() {
        return "/connectBoardHelp";
    }

    @Override
    public FlexMessage processCommand(String message) {
        return getConnectBoardHelp();
    }

    private static FlexMessage getConnectBoardHelp() {
        return new ConnectBoardHelpFlexMessage().get();
    }

    @Override
    public String getDetail() {
        return "Command untuk menampilkan bantuan connect board dalam menggunakan Bot Temanai.\n"
                + "How to use:\n/connectBoardHelp";
    }
}
