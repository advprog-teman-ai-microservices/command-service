package ai.teman.commandservice.core.command.basic.help;

import ai.teman.commandservice.core.command.basic.BasicFlexMessageCommand;
import ai.teman.commandservice.core.command.basic.help.flex_message.DailyMotivationHelpFlexMessage;
import com.linecorp.bot.model.message.FlexMessage;

public class DailyMotivationHelpCommand implements BasicFlexMessageCommand {
    @Override
    public String getName() {
        return "/dailyMotivationHelp";
    }

    @Override
    public FlexMessage processCommand(String message) {
        return getDailyMotivationHelp();
    }

    private static FlexMessage getDailyMotivationHelp() {
        return new DailyMotivationHelpFlexMessage().get();
    }

    @Override
    public String getDetail() {
        return "Command untuk menampilkan bantuan daily motivation dalam menggunakan Bot Temanai.\n"
                + "How to use:\n/dailyMotivationHelp";
    }
}
