package ai.teman.commandservice.core.command.basic.help;

import ai.teman.commandservice.core.command.basic.BasicFlexMessageCommand;
import ai.teman.commandservice.core.command.basic.help.flex_message.MoveCardHelpFlexMessage;
import com.linecorp.bot.model.message.FlexMessage;

public class MoveCardHelpCommand implements BasicFlexMessageCommand {
    @Override
    public String getName() {
        return "/moveCardHelp";
    }

    @Override
    public FlexMessage processCommand(String message) {
        return getMoveCardHelp();
    }

    private static FlexMessage getMoveCardHelp() {
        return new MoveCardHelpFlexMessage().get();
    }

    @Override
    public String getDetail() {
        return "Command untuk menampilkan bantuan move card dalam menggunakan Bot Temanai.\n"
                + "How to use:\n/moveCardHelp";
    }
}
