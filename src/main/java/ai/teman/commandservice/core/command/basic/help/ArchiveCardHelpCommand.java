package ai.teman.commandservice.core.command.basic.help;

import ai.teman.commandservice.core.command.basic.BasicFlexMessageCommand;
import ai.teman.commandservice.core.command.basic.help.flex_message.ArchiveCardHelpFlexMessage;
import com.linecorp.bot.model.message.FlexMessage;

public class ArchiveCardHelpCommand implements BasicFlexMessageCommand {
    @Override
    public String getName() {
        return "/archiveCardHelp";
    }

    @Override
    public FlexMessage processCommand(String message) {
        return getArchiveCardHelp();
    }

    private static FlexMessage getArchiveCardHelp() {
        return new ArchiveCardHelpFlexMessage().get();
    }

    @Override
    public String getDetail() {
        return "Command untuk menampilkan bantuan archive card dalam menggunakan Bot Temanai.\n"
                + "How to use:\n/archiveCardHelp";
    }
}
