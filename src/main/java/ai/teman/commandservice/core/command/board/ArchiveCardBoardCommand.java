package ai.teman.commandservice.core.command.board;

import ai.teman.commandservice.core.builder.FlexMessageBuilder;
import ai.teman.commandservice.core.trello.TrelloApi;
import ai.teman.commandservice.core.trello.TrelloCard;
import ai.teman.commandservice.core.trello.CardNotFoundException;
import ai.teman.commandservice.core.trello.ListNotFoundException;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;

public class ArchiveCardBoardCommand implements BoardCommand {

    @Override
    public String getName() {
        return "/archiveCard";
    }

    @Override
    public Message processCommand(String message, Source chatSource, String boardId) {
        String[] messageLst = message.split("\n");
        if (messageLst.length == 3) {
            String listName = messageLst[1];
            String cardName = messageLst[2];

            try {
                TrelloCard card = TrelloApi.getInstance().getCard(boardId, listName, cardName);
                card.closed = true;
                TrelloApi.getInstance().updateCard(card.id, card);

                String msg = "Card " + cardName + " is successfully archived";
                FlexMessageBuilder builder = new FlexMessageBuilder(msg);
                return builder.createTitleText("Archive Card")
                        .createSeparatorLine()
                        .createBodyText(msg)
                        .buildFlexMessage()
                        .get();
            } catch (ListNotFoundException e) {
                return new TextMessage("List " + listName + " is not found");
            } catch (CardNotFoundException e) {
                return new TextMessage("Card " + cardName + " is not found");
            }
        } else {
            return new TextMessage("Invalid argument for /archiveCard. Read /help");
        }
    }

    @Override
    public String getDetail() {
        return "Command untuk mengarchive sebuah card di Trello.\n"
                + "How to use:\n/archiveCard\nListName\nCardName\n";
    }

}
