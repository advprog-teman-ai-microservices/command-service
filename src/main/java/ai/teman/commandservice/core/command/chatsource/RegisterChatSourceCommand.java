package ai.teman.commandservice.core.command.chatsource;

import ai.teman.commandservice.core.builder.FlexMessageBuilder;
import ai.teman.commandservice.data.Member;
import ai.teman.commandservice.repository.MemberRepo;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;

public class RegisterChatSourceCommand implements ChatSourceCommand {

    private MemberRepo memberRepo;

    public RegisterChatSourceCommand(MemberRepo memberRepo) {
        this.memberRepo = memberRepo;
    }

    @Override
    public String getName() {
        return "/register";
    }

    @Override
    public Message processCommand(String message, Source chatSource) {
        String[] messageLst = message.split(" ");
        if (messageLst.length == 2) {
            Member member = new Member(chatSource.getUserId(), messageLst[1]);
            memberRepo.save(member);

            String msg = "User " + messageLst[1] + " is successfully registered";
            FlexMessageBuilder builder = new FlexMessageBuilder(msg);
            return builder.createTitleText("Register Chat")
                    .createSeparatorLine()
                    .createBodyText(msg)
                    .buildFlexMessage()
                    .get();
        } else {
            return new TextMessage("Invalid argument for /register. Read /help");
        }
    }

    @Override
    public String getDetail() {
        return "Command untuk meregister username Trello ke Teman.ai.\n"
                + "How to use:\n/register <username>\n"
                + "Contoh : /register meow001";
    }

}
