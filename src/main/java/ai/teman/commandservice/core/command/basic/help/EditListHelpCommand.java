package ai.teman.commandservice.core.command.basic.help;

import ai.teman.commandservice.core.command.basic.BasicFlexMessageCommand;
import ai.teman.commandservice.core.command.basic.help.flex_message.EditListHelpFlexMessage;
import com.linecorp.bot.model.message.FlexMessage;

public class EditListHelpCommand implements BasicFlexMessageCommand {
    @Override
    public String getName() {
        return "/editListHelp";
    }

    @Override
    public FlexMessage processCommand(String message) {
        return getEditListHelp();
    }

    private static FlexMessage getEditListHelp() {
        return new EditListHelpFlexMessage().get();
    }

    @Override
    public String getDetail() {
        return "Command untuk menampilkan bantuan edit list dalam menggunakan Bot Temanai.\n"
                + "How to use:\n/editListHelp";
    }
}
