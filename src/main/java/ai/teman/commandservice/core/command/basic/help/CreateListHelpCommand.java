package ai.teman.commandservice.core.command.basic.help;

import ai.teman.commandservice.core.command.basic.BasicFlexMessageCommand;
import ai.teman.commandservice.core.command.basic.help.flex_message.CreateListHelpFlexMessage;
import com.linecorp.bot.model.message.FlexMessage;

public class CreateListHelpCommand implements BasicFlexMessageCommand {
    @Override
    public String getName() {
        return "/createListHelp";
    }

    @Override
    public FlexMessage processCommand(String message) {
        return getCreateListHelp();
    }

    private static FlexMessage getCreateListHelp() {
        return new CreateListHelpFlexMessage().get();
    }

    @Override
    public String getDetail() {
        return "Command untuk menampilkan bantuan create list dalam menggunakan Bot Temanai.\n"
                + "How to use:\n/createListHelp";
    }
}
