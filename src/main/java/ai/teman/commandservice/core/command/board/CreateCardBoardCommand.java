package ai.teman.commandservice.core.command.board;

import ai.teman.commandservice.core.builder.FlexMessageBuilder;
import ai.teman.commandservice.core.trello.TrelloApi;
import ai.teman.commandservice.core.trello.TrelloCard;
import ai.teman.commandservice.core.trello.TrelloList;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CreateCardBoardCommand implements BoardCommand {

    @Override
    public String getName() {
        return "/createCard";
    }

    @Override
    public Message processCommand(String message, Source chatSource, String boardId) {
        String[] messageLst = message.split("\n");
        if (messageLst.length > 4 || messageLst.length < 3) {
            return new TextMessage("Invalid argument for /createCard. See /help for details.");
        }

        Date due = null;
        String listName = messageLst[1];
        String cardName = messageLst[2];

        if (messageLst.length > 3) {
            String dateStr = messageLst[3];
            try {
                due = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(dateStr);
            } catch (ParseException e) {
                return new TextMessage(String.format("'%s' is invalid date format. "
                        + "Please use: 'yyyy-MM-dd HH:mm'.%n", dateStr)
                        + "Example: 2020-12-31 23:59.");
            }
        }

        TrelloList[] lists = TrelloApi.getInstance().getBoardLists(boardId);

        for (TrelloList list : lists) {
            if (list.name.equals(listName)) {
                TrelloCard newCard = new TrelloCard();
                newCard.name = cardName;
                newCard.idBoard = list.idBoard;
                newCard.idList = list.id;
                newCard.due = due;

                TrelloApi.getInstance().createCard(newCard);
                String result = String.format("Card '%s' is successfully created "
                        + "on list '%s' !", newCard.name, list.name);

                FlexMessageBuilder builder = new FlexMessageBuilder(result);
                String dueString = null;
                if (due == null){
                    dueString = "none";
                } else {
                    dueString = due.toString();
                }

                return builder.createTitleText("Create Card")
                        .createBodyText("Card berhasil dibuat! Beberapa info dari Card yang dibuat:")
                        .createSeparatorLine()
                        .createBodyText(
                                String.format("- Card Name: %s%n%n- List Name: %s%n%n- Board ID: %s%n%n- Due Date: %s",
                                        newCard.name, listName, boardId, dueString))
                        .buildFlexMessage()
                        .get();

            }
        }

        return new TextMessage(String.format("List '%s' is not found on the connected board.", listName));
    }

    @Override
    public String getDetail() {
        return "Command untuk membuat sebuah card di suatu list pada Trello.\n"
                + "How to use:\n/createCard\nnamaList\nnamaCard\n(optional) tanggalDue"
                + "tanggalDue harus berformat 'yyyy-MM-dd HH:mm'.\n"
                + "Contoh: 2020-12-31 23:59";
    }
}
