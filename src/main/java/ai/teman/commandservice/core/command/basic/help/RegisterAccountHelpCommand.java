package ai.teman.commandservice.core.command.basic.help;

import ai.teman.commandservice.core.command.basic.BasicFlexMessageCommand;
import ai.teman.commandservice.core.command.basic.help.flex_message.RegisterAccountHelpFlexMessage;
import com.linecorp.bot.model.message.FlexMessage;

public class RegisterAccountHelpCommand implements BasicFlexMessageCommand {
    @Override
    public String getName() {
        return "/registerAccountHelp";
    }

    @Override
    public FlexMessage processCommand(String message) {
        return getRegisterAccountHelp();
    }

    private static FlexMessage getRegisterAccountHelp() {
        return new RegisterAccountHelpFlexMessage().get();
    }

    @Override
    public String getDetail() {
        return "Command untuk menampilkan bantuan register account dalam menggunakan Bot Temanai.\n"
                + "How to use:\n/registerAccountHelp";
    }
}
