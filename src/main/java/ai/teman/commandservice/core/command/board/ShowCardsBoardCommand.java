package ai.teman.commandservice.core.command.board;

import ai.teman.commandservice.core.trello.TrelloApi;
import ai.teman.commandservice.core.trello.TrelloList;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;

public class ShowCardsBoardCommand implements BoardCommand {

    @Override
    public String getName() {
        return "/showCards";
    }

    @Override
    public Message processCommand(String message, Source chatSource, String boardId) {
        String[] messageLstSplitBySpace = message.split(" ");
        String[] messageLstSplitByNewLine = message.split("\n");
        TrelloList[] trelloLists = TrelloApi.getInstance().getBoardLists(boardId);

        if (messageLstSplitBySpace.length == 1 && messageLstSplitByNewLine.length == 1) {
            if (trelloLists.length == 0) {
                return new TextMessage("You don't have ToDo yet");
            } else if (trelloLists.length > 10) {
                return new TextMessage("Sorry, you have more than 10 lists. " +
                        "We are unable to display them because of limitations");
            } else {
                return getShowCards(trelloLists);
            }
        } else {
            return new TextMessage("Invalid argument for /showCards. See /help for details");
        }
    }

    private static FlexMessage getShowCards(TrelloList[] trelloLists) {
        return new ShowCardsFlexMessage().get(trelloLists);
    }

    @Override
    public String getDetail() {
        return "Command untuk menampilkan semua card dari setiap list di Trello.\n"
                + "How to use:\n/showCards";
    }
}