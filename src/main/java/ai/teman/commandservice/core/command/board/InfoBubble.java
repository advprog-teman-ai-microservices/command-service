package ai.teman.commandservice.core.command.board;

import ai.teman.commandservice.core.trello.TrelloCard;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.FlexComponent;
import com.linecorp.bot.model.message.flex.component.Separator;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class InfoBubble {
    private String darkBlueColor = "#094C72";
    private String lightBlueColor = "#5BA6CF";
    private String blackColor = "#000000";
    private String greenColor = "#4CA121";
    private String redColor = "#F4896E";

    public Bubble createBubbleContainsCard(String textHeader, List<TrelloCard> textBody) {
        final Box headerBlock = createHeaderBlock(textHeader);
        final Box bodyBlock = createBodyBlock(textBody);
        return Bubble.builder()
                .header(headerBlock)
                .body(bodyBlock)
                .build();
    }

    public  Bubble createBubbleNotContainsCard (String textHeader, String text) {
        final Box headerBlock = createHeaderBlock(textHeader);
        final Box bodyBlock = createTextBlock(text);
        return Bubble.builder()
                .header(headerBlock)
                .body(bodyBlock)
                .build();
    }

    private Box createHeaderBlock(String textHeader) {
        List<FlexComponent> headerBody = new ArrayList<>();

        headerBody.add(
                Text.builder()
                        .text(textHeader)
                        .size(FlexFontSize.XL)
                        .align(FlexAlign.CENTER)
                        .gravity(FlexGravity.CENTER)
                        .weight(Text.TextWeight.BOLD)
                        .color(darkBlueColor)
                        .build());

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .spacing(FlexMarginSize.MD)
                .contents(headerBody)
                .build();
    }

    private Box createBodyBlock(List<TrelloCard> textBody) {
        List<FlexComponent> bodyContents = new ArrayList<>();

        for (TrelloCard card : textBody) {
            bodyContents.add(cardDetails(card));
        }

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .spacing(FlexMarginSize.MD)
                .contents(bodyContents)
                .build();
    }

    private Box cardDetails(TrelloCard card) {
        Date now = new Date();
        List<FlexComponent> bodyDetails = new ArrayList<>();

        bodyDetails.add(
                Separator.builder()
                        .color(darkBlueColor)
                        .build());

        bodyDetails.add(
                Text.builder()
                        .text(card.name)
                        .size(FlexFontSize.LG)
                        .align(FlexAlign.START)
                        .gravity(FlexGravity.CENTER)
                        .weight(Text.TextWeight.BOLD)
                        .color(lightBlueColor)
                        .wrap(true)
                        .build());

        if (card.due != null) {
            bodyDetails.add(
                    Text.builder()
                            .text("Due Date : " + card.due.toString())
                            .size(FlexFontSize.Md)
                            .align(FlexAlign.START)
                            .gravity(FlexGravity.CENTER)
                            .weight(Text.TextWeight.REGULAR)
                            .color(blackColor)
                            .wrap(true)
                            .build());

            if (!card.dueComplete && now.after(card.due)) {
                bodyDetails.add(
                        Text.builder()
                                .text("OVERDUE")
                                .size(FlexFontSize.Md)
                                .align(FlexAlign.START)
                                .gravity(FlexGravity.CENTER)
                                .weight(Text.TextWeight.BOLD)
                                .color(redColor)
                                .wrap(true)
                                .build());
            }

            if (card.dueComplete) {
                bodyDetails.add(
                        Text.builder()
                                .text("COMPLETE")
                                .size(FlexFontSize.Md)
                                .align(FlexAlign.START)
                                .gravity(FlexGravity.CENTER)
                                .weight(Text.TextWeight.BOLD)
                                .color(greenColor)
                                .wrap(true)
                                .build());
            }
        }

        bodyDetails.add(
                Separator.builder()
                        .color(darkBlueColor)
                        .build());

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .spacing(FlexMarginSize.MD)
                .contents(bodyDetails)
                .build();
    }

    private Box createTextBlock (String textInfo) {
        List<FlexComponent> bodyDetails = new ArrayList<>();

        bodyDetails.add(
                Text.builder()
                        .text(textInfo)
                        .size(FlexFontSize.Md)
                        .align(FlexAlign.START)
                        .gravity(FlexGravity.CENTER)
                        .weight(Text.TextWeight.REGULAR)
                        .color(blackColor)
                        .wrap(true)
                        .build());

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .spacing(FlexMarginSize.MD)
                .contents(bodyDetails)
                .build();
    }
}
