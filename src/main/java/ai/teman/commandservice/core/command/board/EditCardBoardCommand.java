package ai.teman.commandservice.core.command.board;

import ai.teman.commandservice.core.builder.FlexMessageBuilder;
import ai.teman.commandservice.core.trello.CardNotFoundException;
import ai.teman.commandservice.core.trello.ListNotFoundException;
import ai.teman.commandservice.core.trello.TrelloApi;
import ai.teman.commandservice.core.trello.TrelloCard;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EditCardBoardCommand implements BoardCommand {

    @Override
    public String getName() {
        return "/editCard";
    }

    @Override
    public Message processCommand(String message, Source chatSource, String boardId) {
        String[] messageLst = message.split("\n");
        if (messageLst.length >= 4 && messageLst.length <= 5) {
            String listName = messageLst[1];
            String oldCardName = messageLst[2];
            String newCardName = messageLst[3];
            Date due = null;

            if (messageLst.length > 4) {
                String dateStr = messageLst[4];
                try {
                    due = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(dateStr);
                } catch (ParseException e) {
                    return new TextMessage(String.format("'%s' is invalid date format. "
                            + "Please use: 'yyyy-MM-dd HH:mm'.%n", dateStr)
                            + "Example: 2020-12-31 23:59.");
                }
            }

            try {
                TrelloCard card = TrelloApi.getInstance().getCard(boardId, listName, oldCardName);
                card.name = newCardName;
                if (due != null)
                    card.due = due;
                TrelloApi.getInstance().updateCard(card.id, card);

                String msg = "Card " + oldCardName + " is successfully updated";
                FlexMessageBuilder builder = new FlexMessageBuilder(msg);
                return builder.createTitleText("Edit Card")
                        .createSeparatorLine()
                        .createBodyText(msg)
                        .buildFlexMessage()
                        .get();
            } catch (ListNotFoundException e) {
                return new TextMessage("List " + listName + " is not found");
            } catch (CardNotFoundException e) {
                return new TextMessage("Card " + oldCardName + " is not found");
            }
        } else {
            return new TextMessage("Invalid argument for /editCard. Read /help");
        }
    }

    @Override
    public String getDetail() {
        return "Command untuk mengedit sebuah card di Trello.\n"
                + "How to use:\n/editCard\nListName\nOldCardName\nNewCardName\nDue Date (optional)\n";
    }

}
