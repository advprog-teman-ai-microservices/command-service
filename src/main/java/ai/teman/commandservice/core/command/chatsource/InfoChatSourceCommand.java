package ai.teman.commandservice.core.command.chatsource;

import ai.teman.commandservice.core.builder.FlexMessageBuilder;
import ai.teman.commandservice.core.trello.TrelloApi;
import ai.teman.commandservice.core.trello.TrelloBoard;
import ai.teman.commandservice.repository.BoardRoomRepo;
import ai.teman.commandservice.repository.MemberRepo;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.Message;

public class InfoChatSourceCommand implements ChatSourceCommand {

    private BoardRoomRepo boardRoomRepo;
    private MemberRepo memberRepo;

    public InfoChatSourceCommand(BoardRoomRepo boardRoomRepo, MemberRepo memberRepo) {
        this.boardRoomRepo = boardRoomRepo;
        this.memberRepo = memberRepo;
    }

    @Override
    public Message processCommand(String message, Source chatSource) {
        String msg = "";
        String userId = chatSource.getUserId();
        String senderId = chatSource.getSenderId();

        if (memberRepo.existsById(userId)) {
            String username = memberRepo.getOne(chatSource.getUserId()).getUsername();
            msg = "Username: " + username;
        }

        if (boardRoomRepo.existsById(senderId)) {
            String boardId = boardRoomRepo.getOne(senderId).getBoardId();
            if (!msg.equals("")) msg += '\n';

            TrelloBoard boardInfo = TrelloApi.getInstance().getBoardInfo(boardId);
            msg += "Board Name: " + boardInfo.name;
            msg += "\nBoard URL: " + boardInfo.shortUrl;
            msg += "\nBoard ID: " + boardInfo.id;
        }

        FlexMessageBuilder builder = new FlexMessageBuilder(msg);
        return builder.createTitleText("Info")
                .createSeparatorLine()
                .createBodyText(msg)
                .buildFlexMessage()
                .get();
    }

    @Override
    public String getName() {
        return "/info";
    }

    @Override
    public String getDetail() {
        return "Command untuk menampilkan informasi tentang user dan Trello Board.\n"
                + "How to use:\n/showCards";
    }
}
