package ai.teman.commandservice.core.command.basic.help.flex_message;

import com.linecorp.bot.model.action.PostbackAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.*;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.*;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class ArchiveCardHelpFlexMessage implements Supplier<FlexMessage> {
    @Override
    public FlexMessage get() {
        final Box body = createBody();
        final Bubble bubble =
                Bubble.builder()
                        .body(body)
                        .build();
        return new FlexMessage("Help - Archive Card", bubble);
    }

    private Box createBody() {
        List<FlexComponent> bodyContents = new ArrayList<>();

        bodyContents.add(
                Text.builder()
                        .text("Archive Card")
                        .size(FlexFontSize.LG)
                        .align(FlexAlign.CENTER)
                        .gravity(FlexGravity.CENTER)
                        .weight(Text.TextWeight.BOLD)
                        .color("#094C72")
                        .build());

        bodyContents.add(
                Image.builder()
                        .url(URI.create("https://i.ibb.co/0J13dxz/temanai.png"))
                        .size(Image.ImageSize.LG)
                        .aspectRatio(Image.ImageAspectRatio.R1TO1)
                        .aspectMode(Image.ImageAspectMode.Fit)
                        .build());

        bodyContents.add(
                Separator.builder()
                        .color("#000000")
                        .build());

        bodyContents
                .add(createInfoText(
                        "Archive the card which is no longer needed\n\n" +
                                "How to use:"
                ));
        bodyContents
                .add(createCommandText("/archiveCard\nListName\nCardName"));

        bodyContents
                .add(createHelpButton());

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .spacing(FlexMarginSize.SM)
                .contents(bodyContents)
                .build();
    }

    private Text createInfoText(String name) {
        return Text.builder()
                .text(name)
                .size(FlexFontSize.SM)
                .align(FlexAlign.START)
                .gravity(FlexGravity.CENTER)
                .weight(Text.TextWeight.REGULAR)
                .color("#000000")
                .wrap(true)
                .build();
    }

    private Text createCommandText(String name) {
        return Text.builder()
                .text(name)
                .size(FlexFontSize.SM)
                .align(FlexAlign.START)
                .gravity(FlexGravity.CENTER)
                .weight(Text.TextWeight.REGULAR)
                .color("#715180")
                .wrap(true)
                .build();
    }

    PostbackAction helpCommand = new PostbackAction("Back to Help", "/help");
    private Button createHelpButton() {
        return Button.builder()
                .style(Button.ButtonStyle.PRIMARY)
                .height(Button.ButtonHeight.SMALL)
                .color("#5BA6CF")
                .action(helpCommand)
                .build();
    }
}
