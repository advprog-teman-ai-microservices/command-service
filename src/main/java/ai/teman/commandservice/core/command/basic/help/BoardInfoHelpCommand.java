package ai.teman.commandservice.core.command.basic.help;

import ai.teman.commandservice.core.command.basic.BasicFlexMessageCommand;
import ai.teman.commandservice.core.command.basic.help.flex_message.BoardInfoHelpFlexMessage;
import com.linecorp.bot.model.message.FlexMessage;

public class BoardInfoHelpCommand implements BasicFlexMessageCommand {
    @Override
    public String getName() {
        return "/boardInfoHelp";
    }

    @Override
    public FlexMessage processCommand(String message) {
        return getBoardInfoHelp();
    }

    private static FlexMessage getBoardInfoHelp() {
        return new BoardInfoHelpFlexMessage().get();
    }

    @Override
    public String getDetail() {
        return "Command untuk menampilkan bantuan board info dalam menggunakan Bot Temanai.\n"
                + "How to use:\n/boardInfoHelp";
    }
}
