package ai.teman.commandservice.core.command.basic.help;

import ai.teman.commandservice.core.command.basic.BasicFlexMessageCommand;
import ai.teman.commandservice.core.command.basic.help.flex_message.DailyMotivationHelpFlexMessage;
import ai.teman.commandservice.core.command.basic.help.flex_message.FunFactHelpFlexMessage;
import com.linecorp.bot.model.message.FlexMessage;

public class FunFactHelpCommand implements BasicFlexMessageCommand {
    @Override
    public String getName() {
        return "/funFactHelp";
    }

    @Override
    public FlexMessage processCommand(String message) {
        return getFunFactHelp();
    }

    private static FlexMessage getFunFactHelp() {
        return new FunFactHelpFlexMessage().get();
    }

    @Override
    public String getDetail() {
        return "Command untuk menampilkan bantuan fun fact dalam menggunakan Bot Temanai.\n"
                + "How to use:\n/funFactHelp";
    }
}
