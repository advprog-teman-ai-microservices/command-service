package ai.teman.commandservice.core.command.board;

import ai.teman.commandservice.core.builder.FlexMessageBuilder;
import ai.teman.commandservice.core.trello.TrelloApi;
import ai.teman.commandservice.core.trello.TrelloCard;
import ai.teman.commandservice.core.trello.TrelloList;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;

public class MoveCardBoardCommand implements BoardCommand {

    @Override
    public String getName() {
        return "/move";
    }

    @Override
    public Message processCommand(String message, Source chatSource, String boardId) {
        String[] messageLst = message.split("\n");
        if (messageLst.length != 4) {
            return new TextMessage("Invalid argument for /move. See /help for details.");
        }

        String oldListName = messageLst[1];
        String newListName = messageLst[3];
        String cardName = messageLst[2];

        if (oldListName.equals(newListName)) {
            return new TextMessage("Error: Old list and new list have the same name.");
        }

        TrelloList[] lists = TrelloApi.getInstance().getBoardLists(boardId);

        TrelloList oldList = null;
        TrelloList newList = null;
        for (TrelloList list : lists) {
            if (list.name.equals(oldListName)) {
                oldList = list;
            }

            if (list.name.equals(newListName)) {
                newList = list;
            }
        }

        if (oldList == null) {
            return new TextMessage(String.format("List '%s' not found on connected board.", oldListName));
        }

        if (newList == null) {
            return new TextMessage(String.format("List '%s' not found on connected board.", newListName));
        }

        TrelloCard[] cards = TrelloApi.getInstance().getListCards(oldList.id);

        for (TrelloCard card : cards) {
            if (card.name.equals(cardName)) {
                card.idList = newList.id;

                TrelloApi.getInstance().updateCard(card.id, card);
                String result = String.format("Card '%s' on list '%s' is successfully moved to list '%s'.",
                        card.name, oldList.name, newList.name);

                FlexMessageBuilder builder = new FlexMessageBuilder(result);
                return builder.createTitleText("Move Card")
                        .createBodyText("Pemindahan Card berhasil! " + String.format("Card '%s' berhasil dipindah dari List '%s' ke List '%s'!",
                                card.name, oldList.name, newList.name))
                        .buildFlexMessage()
                        .get();
            }
        }

        return new TextMessage(String.format("Card '%s' is not found on list '%s'.",
                cardName, oldList.name));
    }

    @Override
    public String getDetail() {
        return "Command untuk memindahkan sebuah card di suatu list ke list lain pada Trello.\n"
                + "How to use:\n/move\namaListLama\nnamaCard\nnamaListBaru\n"
                + "Kedua List harus ada di board, "
                + "dan namaListLama harus mempunyai card yang ingin dipindah";
    }
}
