package ai.teman.commandservice.core.command.board;

import ai.teman.commandservice.core.builder.FlexMessageBuilder;
import ai.teman.commandservice.core.trello.TrelloApi;
import ai.teman.commandservice.core.trello.TrelloList;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.springframework.web.client.HttpClientErrorException;

public class CreateListBoardCommand implements BoardCommand {

    @Override
    public String getName() {
        return "/createList";
    }

    @Override
    public Message processCommand(String message, Source chatSource, String boardId) {
        String[] messageLst = message.split("\n");
        if (messageLst.length == 2){
            try{
                TrelloList newList = new TrelloList();
                newList.name = messageLst[1];
                newList.idBoard = boardId;
                TrelloApi.getInstance().createList(newList);

                String result = String.format("List '%s' is successfully created", newList.name);

                FlexMessageBuilder builder = new FlexMessageBuilder(result);
                return builder.createTitleText("Create List")
                        .createBodyText("List is successfully created! Some info about the List:")
                        .createSeparatorLine()
                        .createBodyText(
                                String.format("- List Name: %s%n%n- Board ID: %s%n",
                                        newList.name, boardId))
                        .buildFlexMessage()
                        .get();
            } catch (final HttpClientErrorException httpClientErrorException) {
                return new TextMessage("idBoard that used in /connectBoard command is invalid");
            }
        } else {
            return new TextMessage("Invalid argument for /createList. See /help for details");
        }
    }

    @Override
    public String getDetail() {
        return "Command untuk membuat sebuah list di Trello.\n"
                + "How to use:\n/createList\nnamaList";
    }
}
