package ai.teman.commandservice.core.command.basic.help;

import ai.teman.commandservice.core.command.basic.BasicFlexMessageCommand;
import ai.teman.commandservice.core.command.basic.help.flex_message.EditCardHelpFlexMessage;
import com.linecorp.bot.model.message.FlexMessage;

public class EditCardHelpCommand implements BasicFlexMessageCommand {
    @Override
    public String getName() {
        return "/editCardHelp";
    }

    @Override
    public FlexMessage processCommand(String message) {
        return getEditCardHelp();
    }

    private static FlexMessage getEditCardHelp() {
        return new EditCardHelpFlexMessage().get();
    }

    @Override
    public String getDetail() {
        return "Command untuk menampilkan bantuan edit card dalam menggunakan Bot Temanai.\n"
                + "How to use:\n/editCardHelp";
    }
}
