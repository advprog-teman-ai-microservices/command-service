package ai.teman.commandservice.core.command.basic;

import ai.teman.commandservice.core.command.base.BaseCommand;
import com.linecorp.bot.model.message.Message;

public interface BasicFlexMessageCommand extends BaseCommand {
    Message processCommand(String message);
}
