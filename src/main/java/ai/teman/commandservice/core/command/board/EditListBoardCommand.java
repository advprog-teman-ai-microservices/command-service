package ai.teman.commandservice.core.command.board;

import ai.teman.commandservice.core.builder.FlexMessageBuilder;
import ai.teman.commandservice.core.trello.*;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;

public class EditListBoardCommand implements BoardCommand {
    @Override
    public String getName() {
        return "/editList";
    }

    @Override
    public Message processCommand(String message, Source chatSource, String boardId) {
        String[] messageList = message.split("\n");
        if (messageList.length == 3) {
            String oldListName = messageList[1];
            String newListName = messageList[2];
            try {
                TrelloList list = TrelloApi.getInstance().getList(boardId, oldListName);
                list.name = newListName;
                TrelloApi.getInstance().updateList(list.id, list);

                String msg = "List " + oldListName + " is successfully updated";
                FlexMessageBuilder builder = new FlexMessageBuilder(msg);
                return builder.createTitleText("Edit List")
                        .createSeparatorLine()
                        .createBodyText(msg)
                        .createBodyText("Information about the list that has been updated")
                        .createInfoText("\uD83D\uDD39 List Name (New)")
                        .createBodyText(newListName)
                        .createInfoText("\uD83D\uDD39 List Name (Old)")
                        .createBodyText(oldListName)
                        .createInfoText("\uD83D\uDD39 Board ID")
                        .createBodyText(boardId)
                        .buildFlexMessage()
                        .get();
            } catch (ListNotFoundException e) {
                return new TextMessage("List " + oldListName + " is not found");
            }
        } else {
            return new TextMessage("Invalid argument for /editList. Read /help");
        }
    }

    @Override
    public String getDetail() {
        return "Command untuk mengedit nama dari sebuah list di Trello.\n"
                + "How to use:\n/editList\nOldName\nNewName";
    }

}
