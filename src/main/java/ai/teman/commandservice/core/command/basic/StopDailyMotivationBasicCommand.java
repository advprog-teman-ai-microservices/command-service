package ai.teman.commandservice.core.command.basic;

import ai.teman.commandservice.core.command.chatsource.DailyMotivationChatSourceCommand;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;

public class StopDailyMotivationBasicCommand implements BasicCommand{

    @Override
    public String getName() {
        return "/stopDailyMotivation";
    }

    @Override
    public Message processCommand(String message) {
        try {
            DailyMotivationChatSourceCommand.getScheduledService().shutdown();
            DailyMotivationChatSourceCommand.setScheduledService(null);
            return new TextMessage("Daily motivation is successfully stopped");
        } catch (NullPointerException e) {
            return new TextMessage("Please setup daily motivation first");
        }
    }

    @Override
    public String getDetail() {
        return "Command untuk memberhentikan pesan motivasi harian.\n"
                + "How to use:\n/stopDailyMotivation";
    }

}
