package ai.teman.commandservice.core.command.base;

public interface BaseCommand {
    String getName();
    String getDetail();
}
