package ai.teman.commandservice.core.command.basic.help;

import ai.teman.commandservice.core.command.basic.BasicFlexMessageCommand;
import ai.teman.commandservice.core.command.basic.help.flex_message.NeedHelpFlexMessage;
import com.linecorp.bot.model.message.FlexMessage;

public class NeedHelpCommand implements BasicFlexMessageCommand {
    @Override
    public String getName() {
        return "/needHelp";
    }

    @Override
    public FlexMessage processCommand(String message) {
        return getHelp();
    }

    private static FlexMessage getHelp() {
        return new NeedHelpFlexMessage().get();
    }

    @Override
    public String getDetail() {
        return "Command untuk menampilkan bantuan help dalam menggunakan Bot Temanai.\n"
                + "How to use:\n/needHelp";
    }
}
