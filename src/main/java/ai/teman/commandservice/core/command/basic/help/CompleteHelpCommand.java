package ai.teman.commandservice.core.command.basic.help;

import ai.teman.commandservice.core.command.basic.BasicFlexMessageCommand;
import ai.teman.commandservice.core.command.basic.help.flex_message.CompleteHelpFlexMessage;
import com.linecorp.bot.model.message.FlexMessage;

public class CompleteHelpCommand implements BasicFlexMessageCommand {
    @Override
    public String getName() {
        return "/completeHelp";
    }

    @Override
    public FlexMessage processCommand(String message) {
        return getCompleteHelp();
    }

    private static FlexMessage getCompleteHelp() {
        return new CompleteHelpFlexMessage().get();
    }

    @Override
    public String getDetail() {
        return "Command untuk menampilkan bantuan complete dalam menggunakan Bot Temanai.\n"
                + "How to use:\n/completeHelp";
    }
}
