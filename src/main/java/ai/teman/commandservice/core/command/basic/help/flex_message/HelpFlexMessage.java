package ai.teman.commandservice.core.command.basic.help.flex_message;

import com.linecorp.bot.model.action.PostbackAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.*;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.*;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class HelpFlexMessage implements Supplier<FlexMessage> {

    @Override
    public FlexMessage get() {
        final Box body = createBody();
        final Bubble bubble =
                Bubble.builder()
                        .body(body)
                        .build();
        return new FlexMessage("Help", bubble);
    }

    private Box createBody() {
        List<FlexComponent> bodyContents = new ArrayList<>();

        bodyContents.add(
                Text.builder()
                        .text("Help - teman.ai")
                        .size(FlexFontSize.XL)
                        .align(FlexAlign.CENTER)
                        .gravity(FlexGravity.CENTER)
                        .weight(Text.TextWeight.BOLD)
                        .color("#094C72")
                        .build());

        bodyContents.add(
                Image.builder()
                    .url(URI.create("https://i.ibb.co/0J13dxz/temanai.png"))
                    .size(Image.ImageSize.LG)
                    .aspectRatio(Image.ImageAspectRatio.R1TO1)
                    .aspectMode(Image.ImageAspectMode.Fit)
                    .build());

        bodyContents.add(
                Separator.builder()
                        .color("#000000")
                        .build());

        bodyContents
                .add(createRegisterAccountHelpButton());
        bodyContents
                .add(createConnectBoardHelpButton());
        bodyContents
                .add(boardInfoHelpButton());
        bodyContents
                .add(createCreateListHelpButton());
        bodyContents
                .add(createEditListHelpButton());
        bodyContents
                .add(createCreateCardHelpButton());
        bodyContents
                .add(createEditCardHelpButton());
        bodyContents
                .add(createShowCardsHelpButton());
        bodyContents
                .add(createCompleteHelpButton());
        bodyContents
                .add(createMoveCardHelpButton());
        bodyContents
                .add(createArchiveCardHelpButton());
        bodyContents
                .add(createDailyMotivationHelpButton());
        bodyContents
                .add(createNeedHelpButton());
        bodyContents
                .add(createFunFactHelpButton());

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .spacing(FlexMarginSize.SM)
                .contents(bodyContents)
                .build();
    }

    PostbackAction registerAccountHelp = new PostbackAction("Register Trello Account", "/registerAccountHelp");
    private Button createRegisterAccountHelpButton() {
        return Button.builder()
                .style(Button.ButtonStyle.PRIMARY)
                .height(Button.ButtonHeight.SMALL)
                .color("#026AA7")
                .action(registerAccountHelp)
                .build();
    }

    PostbackAction connectBoardHelp = new PostbackAction("Connect Board", "/connectBoardHelp");
    private Button createConnectBoardHelpButton() {
        return Button.builder()
                .style(Button.ButtonStyle.PRIMARY)
                .height(Button.ButtonHeight.SMALL)
                .color("#5BA6CF")
                .action(connectBoardHelp)
                .build();
    }

    PostbackAction boardInfoHelp = new PostbackAction("Board Info", "/boardInfoHelp");
    private Button boardInfoHelpButton() {
        return Button.builder()
                .style(Button.ButtonStyle.PRIMARY)
                .height(Button.ButtonHeight.SMALL)
                .color("#026AA7")
                .action(boardInfoHelp)
                .build();
    }

    PostbackAction createListHelp = new PostbackAction("Create List", "/createListHelp");
    private Button createCreateListHelpButton() {
        return Button.builder()
                .style(Button.ButtonStyle.PRIMARY)
                .height(Button.ButtonHeight.SMALL)
                .color("#5BA6CF")
                .action(createListHelp)
                .build();
    }

    PostbackAction editListHelp = new PostbackAction("Edit List", "/editListHelp");
    private Button createEditListHelpButton() {
        return Button.builder()
                .style(Button.ButtonStyle.PRIMARY)
                .height(Button.ButtonHeight.SMALL)
                .color("#026AA7")
                .action(editListHelp)
                .build();
    }

    PostbackAction createCardHelp = new PostbackAction("Create Card", "/createCardHelp");
    private Button createCreateCardHelpButton() {
        return Button.builder()
                .style(Button.ButtonStyle.PRIMARY)
                .height(Button.ButtonHeight.SMALL)
                .color("#5BA6CF")
                .action(createCardHelp)
                .build();
    }

    PostbackAction editCardHelp = new PostbackAction("Edit Card", "/editCardHelp");
    private Button createEditCardHelpButton() {
        return Button.builder()
                .style(Button.ButtonStyle.PRIMARY)
                .height(Button.ButtonHeight.SMALL)
                .color("#026AA7")
                .action(editCardHelp)
                .build();
    }

    PostbackAction showCardsHelp = new PostbackAction("Show Cards", "/showCardsHelp");
    private Button createShowCardsHelpButton() {
        return Button.builder()
                .style(Button.ButtonStyle.PRIMARY)
                .height(Button.ButtonHeight.SMALL)
                .color("#5BA6CF")
                .action(showCardsHelp)
                .build();
    }

    PostbackAction completeHelp = new PostbackAction("Complete", "/completeHelp");
    private Button createCompleteHelpButton() {
        return Button.builder()
                .style(Button.ButtonStyle.PRIMARY)
                .height(Button.ButtonHeight.SMALL)
                .color("#026AA7")
                .action(completeHelp)
                .build();
    }

    PostbackAction moveCardHelp = new PostbackAction("Move Card", "/moveCardHelp");
    private Button createMoveCardHelpButton() {
        return Button.builder()
                .style(Button.ButtonStyle.PRIMARY)
                .height(Button.ButtonHeight.SMALL)
                .color("#5BA6CF")
                .action(moveCardHelp)
                .build();
    }

    PostbackAction archiveCardHelp = new PostbackAction("Archive Card", "/archiveCardHelp");
    private Button createArchiveCardHelpButton() {
        return Button.builder()
                .style(Button.ButtonStyle.PRIMARY)
                .height(Button.ButtonHeight.SMALL)
                .color("#026AA7")
                .action(archiveCardHelp)
                .build();
    }

    PostbackAction dailyMotivationHelp = new PostbackAction("Daily Motivation", "/dailyMotivationHelp");
    private Button createDailyMotivationHelpButton() {
        return Button.builder()
                .style(Button.ButtonStyle.PRIMARY)
                .height(Button.ButtonHeight.SMALL)
                .color("#5BA6CF")
                .action(dailyMotivationHelp)
                .build();
    }

    PostbackAction needHelp = new PostbackAction("Help", "/needHelp");
    private Button createNeedHelpButton() {
        return Button.builder()
                .style(Button.ButtonStyle.PRIMARY)
                .height(Button.ButtonHeight.SMALL)
                .color("#026AA7")
                .action(needHelp)
                .build();
    }

    PostbackAction funFactHelp = new PostbackAction("Fun Fact", "/funFactHelp");
    private Button createFunFactHelpButton() {
        return Button.builder()
                .style(Button.ButtonStyle.PRIMARY)
                .height(Button.ButtonHeight.SMALL)
                .color("#5BA6CF")
                .action(funFactHelp)
                .build();
    }
}