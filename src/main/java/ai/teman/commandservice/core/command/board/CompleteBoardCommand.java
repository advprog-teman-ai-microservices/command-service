package ai.teman.commandservice.core.command.board;

import ai.teman.commandservice.core.builder.FlexMessageBuilder;
import ai.teman.commandservice.core.trello.*;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;

public class CompleteBoardCommand implements BoardCommand {

    @Override
    public String getName() {
        return "/complete";
    }

    @Override
    public Message processCommand(String message, Source chatSource, String boardId) {
        String[] messageList = message.split("\n");
        if (messageList.length == 3) {
            String listName = messageList[1];
            String cardName = messageList[2];
            try {
                TrelloCard card = TrelloApi.getInstance().getCard(boardId, listName, cardName);
                if (card.dueComplete == false) {
                    card.dueComplete = true;
                    TrelloApi.getInstance().updateCard(card.id, card);

                    String msg = "Card " + cardName + " in " + listName + " is successfully completed";
                    FlexMessageBuilder builder = new FlexMessageBuilder(msg);
                    return builder.createTitleText("Complete")
                            .createSeparatorLine()
                            .createBodyText(msg)
                            .createBodyText("Information about the card that has been completed")
                            .createInfoText("\uD83D\uDD39 Card Name")
                            .createBodyText(cardName)
                            .createInfoText("\uD83D\uDD39 List Name")
                            .createBodyText(listName)
                            .createInfoText("\uD83D\uDD39 Board ID")
                            .createBodyText(boardId)
                            .buildFlexMessage()
                            .get();
                } else {
                    return new TextMessage("Card " + cardName + " in " + listName + " is already completed before");
                }
            } catch (ListNotFoundException e) {
                return new TextMessage("List " + listName + " is not found");
            } catch (CardNotFoundException e) {
                return new TextMessage("Card " + cardName + " is not found");
            }
        } else {
            return new TextMessage("Invalid argument for /complete. Read /help");
        }
    }

    @Override
    public String getDetail() {
        return "Command untuk mengubah status task pada card yang memiliki tenggat waktu menjadi 'complete'.\n" +
        "How to use:\n/complete\nListName\nCardName";
    }
}