package ai.teman.commandservice.core.command.chatsource;

import ai.teman.commandservice.controller.CommandServiceController;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.util.logging.Logger;

public class GetDailyMotivationChatSourceCommand implements Runnable {
    private CommandServiceController serviceController;
    DailyMotivationChatSourceCommand dailyMotivation = DailyMotivationChatSourceCommand.getInstance();
    private Logger logger = Logger.getLogger(dailyMotivation.getName());

    public GetDailyMotivationChatSourceCommand (CommandServiceController serviceController){
        this.serviceController = serviceController;
    }

    public void run() {
        try {
            String quotes = dailyMotivation.getDailyMotivation();
            serviceController.sendPushMessage(dailyMotivation.getIdSender(), quotes);
        } catch (JsonProcessingException e) {
            logger.warning("Error ketika memparse json. Kontak developer.");
        }
    }
}
