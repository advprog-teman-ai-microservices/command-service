package ai.teman.commandservice.core.command.basic.help;

import ai.teman.commandservice.core.command.basic.BasicFlexMessageCommand;
import ai.teman.commandservice.core.command.basic.help.flex_message.CreateCardHelpFlexMessage;
import com.linecorp.bot.model.message.FlexMessage;

public class CreateCardHelpCommand implements BasicFlexMessageCommand {
    @Override
    public String getName() {
        return "/createCardHelp";
    }

    @Override
    public FlexMessage processCommand(String message) {
        return getCreateCardHelp();
    }

    private static FlexMessage getCreateCardHelp() {
        return new CreateCardHelpFlexMessage().get();
    }

    @Override
    public String getDetail() {
        return "Command untuk menampilkan bantuan create card dalam menggunakan Bot Temanai.\n"
                + "How to use:\n/createCardHelp";
    }
}
