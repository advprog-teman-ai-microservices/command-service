package ai.teman.commandservice.core.command.chatsource;

import ai.teman.commandservice.controller.CommandServiceController;
import com.linecorp.bot.model.event.source.GroupSource;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.event.source.UnknownSource;
import com.linecorp.bot.model.event.source.UserSource;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public class DailyMotivationChatSourceCommand implements ChatSourceCommand {
    private static DailyMotivationChatSourceCommand instance;
    private ArrayList<String> dailyMotivationList;
    private static ScheduledExecutorService scheduledService;
    private String senderID;
    private CommandServiceController serviceController;

    private DailyMotivationChatSourceCommand(){
        this.dailyMotivationList = new ArrayList<>();
        seedDailyMotivationList();
    }

    public static DailyMotivationChatSourceCommand getInstance() {
        if (instance == null) {
            instance = new DailyMotivationChatSourceCommand();
        }
        return instance;
    }

    private void seedDailyMotivationList() {
        this.dailyMotivationList.add("We’re here to put a dent in the universe. "
                + "Otherwise why else even be here? - Steve Jobs");
        this.dailyMotivationList.add("The secret of life though, is to fall seven times "
                + "and to get up eight times. - Paul Coelho");
        this.dailyMotivationList.add("Your biggest weakness is when you give up and your greatest power is "
                + "when you try one more time. - Thomas A. Edison");
        this.dailyMotivationList.add("Hard work beats talent when talent fails to work hard. - Tim Notke");
        this.dailyMotivationList.add("Build a dream and the dream will build you. - Robert H. Schuller");
        this.dailyMotivationList.add("Be curious about everything. Never stop learning. Never stop growing. "
                + "- Caley Alyssa");
        this.dailyMotivationList.add("There's only one person who could ever make you happy, and that person "
                + "is you. - David Burns");
        this.dailyMotivationList.add("Live in the present and make it so beautiful that it’s worth "
                + "remembering. - Arnold H. Glasow");
        this.dailyMotivationList.add("You don't need to be great to start something. "
                + "Do it now and don't ever put off because the chance may not come twice. - Zig Ziglar");
        this.dailyMotivationList.add("However difficult life may seem, there's always something you can do "
                + "and succeed at. - Stephen Hawking");
    }

    @Override
    public String getName() {
        return "/setDailyMotivation";
    }

    @Override
    public Message processCommand(String message, Source chatSource) {
        String[] messageLst = message.split(" ");
        if (messageLst.length == 2) {
            try {
                if (getScheduledService() != null) {
                    return new TextMessage("Please stop previous daily motivation");
                } else {
                    if (chatSource instanceof UserSource) {
                        senderID = chatSource.getUserId();
                    } else if (chatSource instanceof GroupSource) {
                        senderID = ((GroupSource) chatSource).getGroupId();
                    } else if (chatSource instanceof UnknownSource) {
                        senderID = chatSource.getSenderId();
                    } else {
                        senderID = chatSource.getSenderId();
                    }
                    long minutesFrequency = Long.parseLong(messageLst[1]);
                    long minutesFrequencyInMiliseconds = minutesFrequency * 60000;
                    setThreadPool(1);
                    getScheduledService().scheduleAtFixedRate(
                            new GetDailyMotivationChatSourceCommand(getController()), minutesFrequencyInMiliseconds,
                            minutesFrequencyInMiliseconds, TimeUnit.MILLISECONDS);
                    return new TextMessage("Daily motivation is successfully updated");
                }
            } catch (NumberFormatException e) {
                return new TextMessage("Invalid argument for /setDailyMotivation. See /help for details");
            }
        } else {
            return new TextMessage("Invalid argument for /setDailyMotivation. See /help for details");
        }
    }

    public String getDailyMotivation() {
        return this.dailyMotivationList.get(new Random().nextInt(this.dailyMotivationList.size()));
    }

    public static ScheduledExecutorService getScheduledService() {
        return scheduledService;
    }

    public static void setThreadPool(int corePoolSize){
        scheduledService = Executors.newScheduledThreadPool(corePoolSize);
    }

    public static void setScheduledService(ScheduledExecutorService scheduled) {
        scheduledService = scheduled;
    }

    public String getIdSender(){
        return senderID;
    }

    public void setController(CommandServiceController controller) {
        this.serviceController = controller;
    }

    public CommandServiceController getController() {
        return serviceController;
    }

    @Override
    public String getDetail() {
        return "Command untuk memberikan pesan motivasi harian.\n"
                + "How to use:\n/setDailyMotivation " + "jumlahHari\n"
                + "Contoh : /setDailyMotivation 3\n"
                + "Berarti motivasi akan diberikan dalam waktu 3 hari sekali.";
    }
}
