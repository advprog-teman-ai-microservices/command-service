package ai.teman.commandservice.core.command.basic.help;

import ai.teman.commandservice.core.command.basic.BasicFlexMessageCommand;
import ai.teman.commandservice.core.command.basic.help.flex_message.HelpFlexMessage;
import com.linecorp.bot.model.message.FlexMessage;

public class HelpBasicCommand implements BasicFlexMessageCommand {

    @Override
    public String getName() {
        return "/help";
    }

    @Override
    public FlexMessage processCommand(String message) {
        return getHelp();
    }

    private static FlexMessage getHelp() {
        return new HelpFlexMessage().get();
    }

    @Override
    public String getDetail() {
        return "Command untuk menampilkan bantuan dalam menggunakan Bot Temanai.\n"
                + "How to use:\n/help";
    }
}
