package ai.teman.commandservice.core.command.basic.help;

import ai.teman.commandservice.core.command.basic.BasicFlexMessageCommand;
import ai.teman.commandservice.core.command.basic.help.flex_message.ShowCardsHelpFlexMessage;
import com.linecorp.bot.model.message.FlexMessage;

public class ShowCardsHelpCommand implements BasicFlexMessageCommand {
    @Override
    public String getName() {
        return "/showCardsHelp";
    }

    @Override
    public FlexMessage processCommand(String message) {
        return getShowCardsHelp();
    }

    private static FlexMessage getShowCardsHelp() {
        return new ShowCardsHelpFlexMessage().get();
    }

    @Override
    public String getDetail() {
        return "Command untuk menampilkan bantuan show cards dalam menggunakan Bot Temanai.\n"
                + "How to use:\n/showCardsHelp";
    }
}
