package ai.teman.commandservice.core.command.board;

import ai.teman.commandservice.core.command.base.BaseCommand;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.Message;

public interface BoardCommand extends BaseCommand {
    Message processCommand(String message, Source chatSource, String boardId);
}
