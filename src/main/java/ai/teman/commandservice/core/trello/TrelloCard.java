package ai.teman.commandservice.core.trello;

import java.util.Date;

public class TrelloCard {
    public String id;
    public String name;
    public String idBoard;
    public String idList;
    public boolean closed;
    public Date due;
    public boolean dueComplete;
}
