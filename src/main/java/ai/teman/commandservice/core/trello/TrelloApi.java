package ai.teman.commandservice.core.trello;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

public class TrelloApi {
    private static TrelloApi instance;

    private RestTemplate restTemplate;

    private final String key = System.getenv("TRELLO_API_KEY");
    private final String token = System.getenv("TRELLO_API_TOKEN");

    private TrelloApi() {
        restTemplate = new RestTemplate();
        restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
    }

    public static TrelloApi getInstance() {
        if (instance == null) {
            instance = new TrelloApi();
        }
        return instance;
    }

    private String getSecrets() {
        return "?key=" + key + "&token=" + token;
    }

    public TrelloBoard getBoardInfo(String boardId) {
        String uri = "https://api.trello.com/1/boards/" + boardId + getSecrets();
        return restTemplate.getForObject(uri, TrelloBoard.class);
    }

    public TrelloList[] getBoardLists(String boardId) {
        String uri = "https://api.trello.com/1/boards/" + boardId + "/lists" + getSecrets();
        return restTemplate.getForObject(uri, TrelloList[].class);
    }

    public TrelloCard[] getListCards(String listId) {
        String uri = "https://api.trello.com/1/lists/" + listId + "/cards" + getSecrets();
        return restTemplate.getForObject(uri, TrelloCard[].class);
    }

    public void updateCard(String cardId, TrelloCard updatedCard) {
        String uri = "https://api.trello.com/1/cards/" + cardId + getSecrets();
        try {
            restTemplate.put(uri, convertObjectToJsonHTTPEntity(updatedCard));
        } catch (JsonProcessingException e){
            System.out.println(e.getMessage());
        }
    }

    public void createCard(TrelloCard newCard) {
        String uri = "https://api.trello.com/1/cards/" + getSecrets();
        try {
            restTemplate.postForObject(uri, convertObjectToJsonHTTPEntity(newCard), TrelloCard.class);
        } catch (JsonProcessingException e){
            System.out.println(e.getMessage());
        }
    }

    public void createList(TrelloList newList) {
        String uri = "https://api.trello.com/1/lists/" + getSecrets();
        try {
            restTemplate.postForObject(uri, convertObjectToJsonHTTPEntity(newList), TrelloList.class);
        } catch (JsonProcessingException e){
            System.out.println(e.getMessage());
        }
    }

    public TrelloList getList (String boardId, String listName) throws ListNotFoundException {
        TrelloList[] trelloLists = getBoardLists(boardId);
        for (TrelloList list : trelloLists) {
            if (list.name.equalsIgnoreCase(listName)) {
                return list;
            }
        }
        throw new ListNotFoundException();
    }

    public void updateList(String listId, TrelloList updatedList) {
        String uri = "https://api.trello.com/1/lists/" + listId + getSecrets();
        try {
            restTemplate.put(uri, convertObjectToJsonHTTPEntity(updatedList));
        } catch (JsonProcessingException e){
            System.out.println(e.getMessage());
        }
    }

    public TrelloCard getCard (String boardId, String listName, String cardName) throws
    ListNotFoundException, CardNotFoundException {
        TrelloList list = getList(boardId, listName);
        TrelloCard[] trelloCards = getListCards(list.id);
        for (TrelloCard card : trelloCards) {
            if (card.name.equalsIgnoreCase(cardName))
                return card;
        }
        throw new CardNotFoundException();
    }

    private HttpEntity<String> convertObjectToJsonHTTPEntity(Object o) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        //for test
        mapper.setAnnotationIntrospector(new JacksonAnnotationIntrospector() {

            @Override
            public boolean hasIgnoreMarker(final AnnotatedMember m) {
                return super.hasIgnoreMarker(m) || m.getName().contains("Mockito");
            }
        });
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<>(mapper.writeValueAsString(o), headers);
    }
}

