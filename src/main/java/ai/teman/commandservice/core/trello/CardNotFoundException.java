package ai.teman.commandservice.core.trello;

public class CardNotFoundException extends Exception {
    public CardNotFoundException() {

    }

    public CardNotFoundException(String errorMessage) {
        super(errorMessage);
    }
}
