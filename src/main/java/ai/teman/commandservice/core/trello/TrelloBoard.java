package ai.teman.commandservice.core.trello;

public class TrelloBoard {
    public String id;
    public String name;
    public String desc;
    public String url;
    public String shortUrl;
}
