package ai.teman.commandservice.core.trello;

public class ListNotFoundException extends Exception {
    public ListNotFoundException() {

    }

    public ListNotFoundException(String errorMessage) {
        super(errorMessage);
    }
}
