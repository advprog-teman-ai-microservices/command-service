package ai.teman.commandservice.core.builder;

import com.linecorp.bot.model.action.PostbackAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.*;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.*;

import java.net.URI;
import java.util.ArrayList;
import java.util.function.Supplier;

public class FlexMessageBuilder implements Supplier<FlexMessage> {

    private ArrayList<FlexComponent> bodyContents;
    private String altText;
    private FlexMessage flexMessage;

    public FlexMessageBuilder(String altText) {
        this.altText = altText;
        this.bodyContents = new ArrayList<>();
    }

    public FlexMessageBuilder buildFlexMessage() {
        Box body = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .spacing(FlexMarginSize.SM)
                .contents(bodyContents)
                .build();

        Bubble bubble =
                Bubble.builder()
                        .body(body)
                        .build();

        this.flexMessage = new FlexMessage(altText, bubble);

        return this;
    }

    public FlexMessageBuilder createBodyText(String bodyText) {
        bodyContents.add(
                Text.builder()
                        .text(bodyText)
                        .size(FlexFontSize.SM)
                        .align(FlexAlign.START)
                        .gravity(FlexGravity.CENTER)
                        .weight(Text.TextWeight.REGULAR)
                        .color("#000000")
                        .wrap(true)
                        .build()
        );

        return this;
    }

    public FlexMessageBuilder createTitleText(String titleText) {
        bodyContents.add(
                Text.builder()
                        .text(titleText)
                        .size(FlexFontSize.LG)
                        .align(FlexAlign.CENTER)
                        .gravity(FlexGravity.CENTER)
                        .weight(Text.TextWeight.BOLD)
                        .color("#094C72")
                        .build()
        );

        return this;
    }

    public FlexMessageBuilder createInfoText(String infoText) {
        bodyContents.add(
                Text.builder()
                        .text(infoText)
                        .size(FlexFontSize.SM)
                        .align(FlexAlign.START)
                        .gravity(FlexGravity.CENTER)
                        .weight(Text.TextWeight.REGULAR)
                        .color("#715180")
                        .wrap(true)
                        .build()
        );

        return this;
    }

    public FlexMessageBuilder createHelpButton() {
        PostbackAction helpCommand = new PostbackAction("Back to Help", "/help");
        bodyContents.add(
                Button.builder()
                        .style(Button.ButtonStyle.PRIMARY)
                        .height(Button.ButtonHeight.SMALL)
                        .color("#5BA6CF")
                        .action(helpCommand)
                        .build()
        );

        return this;
    }

    public FlexMessageBuilder createSeparatorLine() {
        bodyContents.add(
                Separator.builder()
                        .color("#000000")
                        .build()
        );

        return this;
    }

    public FlexMessageBuilder createImage(String imageLink) {
        bodyContents.add(
                Image.builder()
                        .url(URI.create(imageLink))
                        .size(Image.ImageSize.LG)
                        .aspectRatio(Image.ImageAspectRatio.R1TO1)
                        .aspectMode(Image.ImageAspectMode.Fit)
                        .build()
        );

        return this;
    }

    public FlexMessageBuilder createTemanaiLogo(){
        return createImage("https://i.ibb.co/0J13dxz/temanai.png");
    }

    @Override
    public FlexMessage get() {
        return this.flexMessage;
    }
    
}
