package ai.teman.commandservice.service;

import ai.teman.commandservice.controller.CommandServiceController;
import ai.teman.commandservice.core.command.basic.FunFactBasicCommand;
import ai.teman.commandservice.core.command.basic.StopDailyMotivationBasicCommand;
import ai.teman.commandservice.core.command.basic.help.*;
import ai.teman.commandservice.core.command.board.*;
import ai.teman.commandservice.core.command.chatsource.ConnectBoardChatSourceCommand;
import ai.teman.commandservice.core.command.chatsource.DailyMotivationChatSourceCommand;
import ai.teman.commandservice.core.command.chatsource.InfoChatSourceCommand;
import ai.teman.commandservice.core.command.chatsource.RegisterChatSourceCommand;
import ai.teman.commandservice.repository.*;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommandServiceImpl implements CommandService {
    private BasicCommandRepo basicCommandRepo;
    private BasicFlexMessageCommandRepo basicFlexMessageCommandRepo;
    private ChatSourceCommandRepo chatSourceCommandRepo;
    private BoardCommandRepo boardCommandRepo;

    private BoardRoomRepo boardRoom;
    private MemberRepo memberRepo;

    @Autowired
    public CommandServiceImpl(
            BasicCommandRepo basicCommandRepo,
            BasicFlexMessageCommandRepo basicFlexMessageCommandRepo,
            ChatSourceCommandRepo chatSourceCommandRepo,
            BoardCommandRepo boardCommandRepo,
            BoardRoomRepo boardRoom,
            MemberRepo memberRepo
    ) {
        this.basicCommandRepo = basicCommandRepo;
        this.basicFlexMessageCommandRepo = basicFlexMessageCommandRepo;
        this.chatSourceCommandRepo = chatSourceCommandRepo;
        this.boardCommandRepo = boardCommandRepo;
        this.boardRoom = boardRoom;
        this.memberRepo = memberRepo;
        seed();
    }

    private void seed() {
        // Register to basicCommandRepo if your Command only needs the message
        basicCommandRepo.registerCommand(new FunFactBasicCommand());
        basicCommandRepo.registerCommand(new StopDailyMotivationBasicCommand());

        // Register to basicFlexMessageCommandRepo if your Command needs flex message
        basicFlexMessageCommandRepo.registerCommand(new HelpBasicCommand());
        basicFlexMessageCommandRepo.registerCommand(new RegisterAccountHelpCommand());
        basicFlexMessageCommandRepo.registerCommand(new ConnectBoardHelpCommand());
        basicFlexMessageCommandRepo.registerCommand(new BoardInfoHelpCommand());
        basicFlexMessageCommandRepo.registerCommand(new CreateListHelpCommand());
        basicFlexMessageCommandRepo.registerCommand(new EditListHelpCommand());
        basicFlexMessageCommandRepo.registerCommand(new CreateCardHelpCommand());
        basicFlexMessageCommandRepo.registerCommand(new EditCardHelpCommand());
        basicFlexMessageCommandRepo.registerCommand(new ShowCardsHelpCommand());
        basicFlexMessageCommandRepo.registerCommand(new CompleteHelpCommand());
        basicFlexMessageCommandRepo.registerCommand(new MoveCardHelpCommand());
        basicFlexMessageCommandRepo.registerCommand(new ArchiveCardHelpCommand());
        basicFlexMessageCommandRepo.registerCommand(new DailyMotivationHelpCommand());
        basicFlexMessageCommandRepo.registerCommand(new NeedHelpCommand());
        basicFlexMessageCommandRepo.registerCommand(new FunFactHelpCommand());

        /* Register to chatSourceCommandRepo if your Command needs the message
         and the sender information */
        chatSourceCommandRepo.registerCommand(new RegisterChatSourceCommand(memberRepo));
        chatSourceCommandRepo.registerCommand(new ConnectBoardChatSourceCommand(boardRoom));
        chatSourceCommandRepo.registerCommand(new InfoChatSourceCommand(boardRoom, memberRepo));

        // Register to boardCommandRepo if your Command needs the message and the board id
        boardCommandRepo.registerCommand(new EditCardBoardCommand());
        boardCommandRepo.registerCommand(new ArchiveCardBoardCommand());
        boardCommandRepo.registerCommand(new MoveCardBoardCommand());
        boardCommandRepo.registerCommand(new CreateCardBoardCommand());
        boardCommandRepo.registerCommand(new EditListBoardCommand());
        boardCommandRepo.registerCommand(new CompleteBoardCommand());
        boardCommandRepo.registerCommand(new CreateListBoardCommand());
        boardCommandRepo.registerCommand(new ShowCardsBoardCommand());
    }

    @Override
    public Message processCommand(String message, Source chatSource) {
        String[] messageLst = message.split("[ \\n]");
        Message resultMessage = basicCommandRepo.processCommand(messageLst[0], message);
        if (resultMessage == null) {
            resultMessage = chatSourceCommandRepo.processCommand(messageLst[0], message, chatSource);
        }
        if (resultMessage == null) {
            resultMessage = boardCommandRepo.processCommand(messageLst[0], message, chatSource);
        }
        if (resultMessage == null){
            resultMessage = basicFlexMessageCommandRepo.processCommand(messageLst[0], message);
        }

        if (resultMessage != null) {
            return resultMessage;
        } else if (message.length() > 1 && message.charAt(0) == '/') {
            return new TextMessage("Command anda gak ketemu");
        } else {
            return null;
        }
    }

    @Override
    public void setController(CommandServiceController obj){
        chatSourceCommandRepo.registerCommand(DailyMotivationChatSourceCommand.getInstance());
        DailyMotivationChatSourceCommand.getInstance().setController(obj);
    }
}
