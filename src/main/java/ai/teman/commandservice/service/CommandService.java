package ai.teman.commandservice.service;

import ai.teman.commandservice.controller.CommandServiceController;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.Message;

public interface CommandService {
    Message processCommand(String message, Source chatSource);
    void setController(CommandServiceController obj);
}
