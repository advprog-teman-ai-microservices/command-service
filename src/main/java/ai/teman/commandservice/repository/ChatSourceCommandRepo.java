package ai.teman.commandservice.repository;

import ai.teman.commandservice.core.command.chatsource.ChatSourceCommand;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.Message;
import org.springframework.stereotype.Repository;

@Repository
public class ChatSourceCommandRepo extends BaseCommandRepo<ChatSourceCommand> {
    /**
     * .
     * @param cmdName
     * @param message
     * @param chatSource
     * @return
     */
    public Message processCommand(String cmdName, String message, Source chatSource) {
        if (mapCommand.containsKey(cmdName)) {
            return mapCommand.get(cmdName).processCommand(message, chatSource);
        } else {
            return null;
        }
    }
}
