package ai.teman.commandservice.repository;

import ai.teman.commandservice.core.command.basic.BasicCommand;
import com.linecorp.bot.model.message.Message;
import org.springframework.stereotype.Repository;

@Repository
public class BasicCommandRepo extends BaseCommandRepo<BasicCommand> {
    public Message processCommand(String cmdName, String message) {
        if (mapCommand.containsKey(cmdName)) {
            return mapCommand.get(cmdName).processCommand(message);
        } else {
            return null;
        }
    }
}
