package ai.teman.commandservice.repository;

import ai.teman.commandservice.data.BoardRoom;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BoardRoomRepo extends JpaRepository<BoardRoom, String> {

}
