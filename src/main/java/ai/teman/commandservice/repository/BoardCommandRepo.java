package ai.teman.commandservice.repository;

import ai.teman.commandservice.core.command.board.BoardCommand;
import ai.teman.commandservice.data.BoardRoom;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public class BoardCommandRepo extends BaseCommandRepo<BoardCommand> {

    private BoardRoomRepo boardRoom;

    @Autowired
    public BoardCommandRepo(BoardRoomRepo boardRoom) {
        super();
        this.boardRoom = boardRoom;
    }

    public Message processCommand(String cmdName, String message, Source chatSource) {
        if (mapCommand.containsKey(cmdName)) {
            Optional<BoardRoom> optBoardRoom = boardRoom.findById(chatSource.getSenderId());
            if (optBoardRoom.isPresent()) {
                return mapCommand.get(cmdName).processCommand(message,
                        chatSource, optBoardRoom.get().getBoardId());
            } else {
                return new TextMessage("Board not found. Please /connectBoard using your Board ID");
            }
        } else {
            return null;
        }
    }
}
