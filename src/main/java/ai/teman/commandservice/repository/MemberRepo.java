package ai.teman.commandservice.repository;

import ai.teman.commandservice.data.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepo extends JpaRepository<Member, String> {

}
