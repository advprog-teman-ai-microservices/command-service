package ai.teman.commandservice.repository;

import ai.teman.commandservice.core.command.base.BaseCommand;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public abstract class BaseCommandRepo<T extends BaseCommand> {
    protected Map<String, T> mapCommand;

    public BaseCommandRepo() {
        mapCommand = new HashMap<>();
    }

    public Collection<T> getCommands() {
        return mapCommand.values();
    }

    public void registerCommand(T t) {
        mapCommand.put(t.getName(), t);
    }
}
