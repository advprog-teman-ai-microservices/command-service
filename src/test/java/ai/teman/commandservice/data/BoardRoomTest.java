package ai.teman.commandservice.data;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class BoardRoomTest {

    private BoardRoom boardRoom;

    @BeforeEach
    void setUp() throws Exception {
        boardRoom = new BoardRoom("meong", "test-01");
    }

    @Test
    void testEmptyConstructor() {
        boardRoom = new BoardRoom();
        assertNotNull(boardRoom);
    }

    @Test
    void testGetId() {
        assertEquals("meong", boardRoom.getId());
    }

    @Test
    void testGetBoardId() {
        assertEquals("test-01", boardRoom.getBoardId());
    }

    @Test
    void testSetId() {
        boardRoom.setId("gukguk");
        assertEquals("gukguk", boardRoom.getId());
    }

    @Test
    void testSetBoardId() {
        boardRoom.setBoardId("super-01");
        assertEquals("super-01", boardRoom.getBoardId());
    }

}
