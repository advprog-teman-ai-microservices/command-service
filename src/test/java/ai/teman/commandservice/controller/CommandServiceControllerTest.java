package ai.teman.commandservice.controller;

import ai.teman.commandservice.service.CommandService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.TextMessage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class CommandServiceControllerTest {

    @InjectMocks
    public CommandServiceController commandServiceController;

    @Mock
    private CommandService commandService;

    @Mock
    private RestTemplate restTemplate;

    @Test
    public void testReplyMessage() throws JsonProcessingException {
        commandService = mock(CommandService.class);
        commandServiceController = new CommandServiceController(commandService, restTemplate);
        Source source = mock(Source.class);
        String message = "tes";
        List<String> list = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);

        //test ketika success parse json string, command service manggil replyMessage
        list.add(0, mapper.writeValueAsString(message));
        list.add(1, mapper.writeValueAsString(source));
        commandServiceController.processCommand(list);
        verify(commandService).processCommand(anyString(), any(Source.class));

        //test error parse json
        list.add(0, mapper.writeValueAsString(2));
        list.add(1, mapper.writeValueAsString(1));
        TextMessage errorParseJson = new TextMessage("Error ketika memparse json. Kontak developer.");
        assertEquals(errorParseJson,
                commandServiceController.processCommand(list));
    }

}
