package ai.teman.commandservice.service;

import ai.teman.commandservice.core.command.basic.FunFactBasicCommand;
import ai.teman.commandservice.core.command.basic.StopDailyMotivationBasicCommand;
import ai.teman.commandservice.core.command.basic.help.*;
import ai.teman.commandservice.core.command.board.*;
import ai.teman.commandservice.core.command.chatsource.ConnectBoardChatSourceCommand;
import ai.teman.commandservice.core.command.chatsource.RegisterChatSourceCommand;
import ai.teman.commandservice.repository.*;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CommandServiceImplTest {

    @Mock
    private BasicCommandRepo basicCommandRepo;

    @Mock
    private BasicFlexMessageCommandRepo basicFlexMessageCommandRepo;

    @Mock
    private ChatSourceCommandRepo chatSourceCommandRepo;

    @Mock
    private BoardCommandRepo boardCommandRepo;

    @Mock
    private BoardRoomRepo boardRoomRepo;

    @Mock
    private MemberRepo memberRepo;


    @InjectMocks
    private CommandServiceImpl service;

    private CommandServiceImpl makeService(){
        return new CommandServiceImpl(
                basicCommandRepo,
                basicFlexMessageCommandRepo,
                chatSourceCommandRepo,
                boardCommandRepo,
                boardRoomRepo,
                memberRepo
        );
    }

    @Test
    public void whenNewServiceIsCreatedItShouldCallSeedAndRegisterAllCommand(){
        makeService();

        verify(basicCommandRepo, atLeastOnce()).registerCommand(any(FunFactBasicCommand.class));
        verify(basicCommandRepo, atLeastOnce()).registerCommand(any(StopDailyMotivationBasicCommand.class));

        verify(basicFlexMessageCommandRepo, atLeastOnce()).registerCommand(any(HelpBasicCommand.class));
        verify(basicFlexMessageCommandRepo, atLeastOnce()).registerCommand(any(RegisterAccountHelpCommand.class));
        verify(basicFlexMessageCommandRepo, atLeastOnce()).registerCommand(any(ConnectBoardHelpCommand.class));
        verify(basicFlexMessageCommandRepo, atLeastOnce()).registerCommand(any(BoardInfoHelpCommand.class));
        verify(basicFlexMessageCommandRepo, atLeastOnce()).registerCommand(any(CreateListHelpCommand.class));
        verify(basicFlexMessageCommandRepo, atLeastOnce()).registerCommand(any(EditListHelpCommand.class));
        verify(basicFlexMessageCommandRepo, atLeastOnce()).registerCommand(any(CreateCardHelpCommand.class));
        verify(basicFlexMessageCommandRepo, atLeastOnce()).registerCommand(any(EditCardHelpCommand.class));
        verify(basicFlexMessageCommandRepo, atLeastOnce()).registerCommand(any(ShowCardsHelpCommand.class));
        verify(basicFlexMessageCommandRepo, atLeastOnce()).registerCommand(any(CompleteHelpCommand.class));
        verify(basicFlexMessageCommandRepo, atLeastOnce()).registerCommand(any(MoveCardHelpCommand.class));
        verify(basicFlexMessageCommandRepo, atLeastOnce()).registerCommand(any(ArchiveCardHelpCommand.class));
        verify(basicFlexMessageCommandRepo, atLeastOnce()).registerCommand(any(NeedHelpCommand.class));
        verify(basicFlexMessageCommandRepo, atLeastOnce()).registerCommand(any(FunFactHelpCommand.class));

        verify(boardCommandRepo, atLeastOnce()).registerCommand(any(CreateCardBoardCommand.class));
        verify(boardCommandRepo, atLeastOnce()).registerCommand(any(ArchiveCardBoardCommand.class));
        verify(boardCommandRepo, atLeastOnce()).registerCommand(any(EditCardBoardCommand.class));
        verify(boardCommandRepo, atLeastOnce()).registerCommand(any(MoveCardBoardCommand.class));
        verify(boardCommandRepo, atLeastOnce()).registerCommand(any(CreateListBoardCommand.class));
        verify(boardCommandRepo, atLeastOnce()).registerCommand(any(ShowCardsBoardCommand.class));
        verify(boardCommandRepo, atLeastOnce()).registerCommand(any(CompleteBoardCommand.class));
        verify(boardCommandRepo, atLeastOnce()).registerCommand(any(EditCardBoardCommand.class));

        verify(chatSourceCommandRepo, atLeastOnce()).registerCommand(any(ConnectBoardChatSourceCommand.class));
        verify(chatSourceCommandRepo, atLeastOnce()).registerCommand(any(RegisterChatSourceCommand.class));
    }

    @Test
    public void whenReplyMessageIsCalledItShouldCallProcessCommandAndCallLineClientReplyMessage() {
        //Mock source
        Source source = mock(Source.class);

        //Tes ketika message tidak diawali '/' maka return null
        assertNull(service.processCommand("test abc", source));

        //Tes ketika message diawali '/' tapi commandnya tidak valid maka mereturn "Command anda gak ketemu"
        TextMessage command_tidak_valid = new TextMessage("Command anda gak ketemu");
        assertEquals(command_tidak_valid,
                service.processCommand("/test abc", source));

        verify(basicCommandRepo).processCommand("/test", "/test abc");
        verify(boardCommandRepo).processCommand("/test", "/test abc", source);
        verify(chatSourceCommandRepo).processCommand("/test", "/test abc", source);

        //Tes ketika message diawali '/' dan commandnya valid maka mereturn hasil processcommand
        when(basicCommandRepo.processCommand("/funFact", "/funFact a")).
                thenReturn(new TextMessage("Sebuah Fun fact"));


        assertEquals("Sebuah Fun fact",
                getTextFromTextMessage(service.processCommand("/funFact a", source)));

        verify(basicCommandRepo).processCommand("/funFact", "/funFact a");

    }

    String getTextFromTextMessage(Message message){
        TextMessage textMessage = (TextMessage) message;
        return textMessage.getText();
    }
}
