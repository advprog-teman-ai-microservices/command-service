package ai.teman.commandservice.core.command.basic;

import ai.teman.commandservice.core.command.chatsource.DailyMotivationChatSourceCommand;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class StopDailyMotivationTest {
    private StopDailyMotivationBasicCommand stopDailyMotivation;

    @BeforeEach
    void setUp() {
        stopDailyMotivation = new StopDailyMotivationBasicCommand();
    }

    @Test
    void testGetName() {
        assertEquals("/stopDailyMotivation", stopDailyMotivation.getName());
    }

    @Test
    void testGetDetail() {
        assertEquals("Command untuk memberhentikan pesan motivasi harian.\n" +
                "How to use:\n/stopDailyMotivation", stopDailyMotivation.getDetail());
    }

    @Test
    void testProcessCommand() {
        DailyMotivationChatSourceCommand.setThreadPool(1);
        assertEquals("Daily motivation is successfully stopped", getTextFromTextMessage(
                stopDailyMotivation.processCommand("/stopDailyMotivation")));

        DailyMotivationChatSourceCommand.setThreadPool(1);
        DailyMotivationChatSourceCommand.getScheduledService().shutdown();
        DailyMotivationChatSourceCommand.setScheduledService(null);
        assertEquals("Please setup daily motivation first", getTextFromTextMessage(
                stopDailyMotivation.processCommand("/stopDailyMotivation")));
    }

    String getTextFromTextMessage(Message message){
        TextMessage textMessage = (TextMessage) message;
        return textMessage.getText();
    }
}
