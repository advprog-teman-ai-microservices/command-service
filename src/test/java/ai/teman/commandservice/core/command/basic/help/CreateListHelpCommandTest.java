package ai.teman.commandservice.core.command.basic.help;

import ai.teman.commandservice.core.command.basic.help.flex_message.CreateListHelpFlexMessage;
import com.linecorp.bot.model.message.FlexMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CreateListHelpCommandTest {
    private CreateListHelpCommand createListHelpCommand;

    @BeforeEach
    public void setUp() {
        createListHelpCommand = new CreateListHelpCommand();
    }

    @Test
    public void testGetName() {
        assertEquals("/createListHelp", createListHelpCommand.getName());
    }

    @Test
    public void testGetDetail() {
        assertEquals("Command untuk menampilkan bantuan create list dalam menggunakan Bot Temanai.\n"
                        + "How to use:\n/createListHelp",
                createListHelpCommand.getDetail());
    }

    @Test
    public void testProcessCommand(){
        FlexMessage createListHelpProcessCommand = createListHelpCommand.processCommand(
                createListHelpCommand.getName());
        CreateListHelpFlexMessage createListHelpFlexMessage = new CreateListHelpFlexMessage();

        assertTrue(createListHelpProcessCommand.equals(createListHelpFlexMessage.get()));
    }
}
