package ai.teman.commandservice.core.command.board;

import ai.teman.commandservice.core.trello.ListNotFoundException;
import ai.teman.commandservice.core.trello.TrelloApi;
import ai.teman.commandservice.core.trello.TrelloList;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.message.FlexMessage;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.linecorp.bot.model.event.source.Source;
import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ShowCardsBoardCommandTest {
    @Mock
    private TrelloApi trelloApi;

    @InjectMocks
    private ShowCardsBoardCommand showCardsCommand;

    @Test
    public void testGetName() {
        assertEquals("/showCards", showCardsCommand.getName());
    }

    @Test
    public void testGetDetail() {
        assertEquals( "Command untuk menampilkan semua card dari setiap list di Trello.\n" +
                "How to use:\n/showCards", showCardsCommand.getDetail());
    }

    //Setup method to mock Singleton class
    //Reference: https://stackoverflow.com/questions/38914433/mocking-a-singleton-with-mockito
    private void setMock(TrelloApi mock) {
        try {
            Field instance = TrelloApi.class.getDeclaredField("instance");
            instance.setAccessible(true);
            instance.set(instance, mock);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void testProcessCommand() {
        //Init mocks
        setMock(trelloApi);
        TrelloList[] trelloList = trelloApi.getBoardLists("boardId");
        TrelloList list = mock(TrelloList.class);

        Source source = mock(Source.class);

        //Test invalid argument
        assertEquals("Invalid argument for /showCards. See /help for details",
                getTextFromTextMessage(showCardsCommand.
                        processCommand("/showCards Advprog", source, "boardTest")));

        assertEquals("Invalid argument for /showCards. See /help for details",
                getTextFromTextMessage(showCardsCommand.
                        processCommand("/showCards\n Advprog", source, "boardTest")));
    }

    String getTextFromTextMessage(Message message){
        TextMessage textMessage = (TextMessage) message;
        return textMessage.getText();
    }

    String getAtlTextFromFlexMessage(Message message){
        FlexMessage flexMessage = (FlexMessage) message;
        return flexMessage.getAltText();
    }
}
