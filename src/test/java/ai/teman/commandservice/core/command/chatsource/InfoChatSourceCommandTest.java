package ai.teman.commandservice.core.command.chatsource;

import ai.teman.commandservice.core.trello.TrelloApi;
import ai.teman.commandservice.core.trello.TrelloBoard;
import ai.teman.commandservice.data.BoardRoom;
import ai.teman.commandservice.data.Member;
import ai.teman.commandservice.repository.BoardRoomRepo;
import ai.teman.commandservice.repository.MemberRepo;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class InfoChatSourceCommandTest {

    @InjectMocks
    private InfoChatSourceCommand infoChat;

    @Mock
    private MemberRepo memberRepo;

    @Mock
    private BoardRoomRepo boardRoomRepo;

    @Mock
    Source chatSource;

    @Mock
    private TrelloApi trelloApi;

    @BeforeEach
    void setUp() {
        infoChat = new InfoChatSourceCommand(boardRoomRepo, memberRepo);
    }

    //Setup method to mock Singleton class
    //Reference: https://stackoverflow.com/questions/38914433/mocking-a-singleton-with-mockito
    private void setMock(TrelloApi mock) {
        try {
            Field instance = TrelloApi.class.getDeclaredField("instance");
            instance.setAccessible(true);
            instance.set(instance, mock);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void testGetName() {
        assertEquals("/info", infoChat.getName());
    }

    @Test
    void testGetDetail() {
        assertNotNull(infoChat.getDetail());
    }

    @Test
    void testProcessCommand() {
        setMock(trelloApi);

        when(chatSource.getSenderId()).thenReturn("aaa");
        when(chatSource.getUserId()).thenReturn("bbb");
        when(memberRepo.existsById(anyString())).thenReturn(true);
        when(boardRoomRepo.existsById(anyString())).thenReturn(true);

        Member member = new Member();
        member.setUsername("alghi");
        when(memberRepo.getOne("bbb")).thenReturn(member);

        BoardRoom boardRoom = new BoardRoom();
        boardRoom.setBoardId("hehe");
        when(boardRoomRepo.getOne("aaa")).thenReturn(boardRoom);

        TrelloBoard boardInfo = new TrelloBoard();
        boardInfo.id = "hehe123456";
        boardInfo.shortUrl = "https://trello.com/b/hehe";
        boardInfo.name = "Board Hehe";
        when(trelloApi.getBoardInfo("hehe")).thenReturn(boardInfo);

        String result = getAtlTextFromFlexMessage(infoChat.processCommand("/info", chatSource));
        assertTrue(result.contains(boardInfo.id));
        assertTrue(result.contains(boardInfo.shortUrl));
        assertTrue(result.contains(boardInfo.name));
    }

    String getAtlTextFromFlexMessage(Message message){
        FlexMessage flexMessage = (FlexMessage) message;
        return flexMessage.getAltText();
    }

    String getTextFromTextMessage(Message message){
        TextMessage textMessage = (TextMessage) message;
        return textMessage.getText();
    }
}
