package ai.teman.commandservice.core.command.basic.help;

import ai.teman.commandservice.core.command.basic.help.flex_message.CreateCardHelpFlexMessage;
import com.linecorp.bot.model.message.FlexMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CreateCardHelpCommandTest {
    private CreateCardHelpCommand createCardHelpCommand;

    @BeforeEach
    public void setUp() {
        createCardHelpCommand = new CreateCardHelpCommand();
    }

    @Test
    public void testGetName() {
        assertEquals("/createCardHelp", createCardHelpCommand.getName());
    }

    @Test
    public void testGetDetail() {
        assertEquals("Command untuk menampilkan bantuan create card dalam menggunakan Bot Temanai.\n"
                        + "How to use:\n/createCardHelp",
                createCardHelpCommand.getDetail());
    }

    @Test
    public void testProcessCommand(){
        FlexMessage createCardHelpProcessCommand = createCardHelpCommand.processCommand(
                createCardHelpCommand.getName());
        CreateCardHelpFlexMessage createCardHelpFlexMessage = new CreateCardHelpFlexMessage();

        assertTrue(createCardHelpProcessCommand.equals(createCardHelpFlexMessage.get()));
    }
}
