package ai.teman.commandservice.core.command.basic;

import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FunFactBasicCommandTest {
    private FunFactBasicCommand funFactCommand;

    @BeforeEach
    public void setUp() {
        funFactCommand = new FunFactBasicCommand();
    }

    @Test
    public void testGetName() {
        assertEquals("/funFact", funFactCommand.getName());
    }

    @Test
    public void testProcessCommand(){
        String funFact = getTextFromTextMessage(funFactCommand.processCommand(""));

        assertTrue(funFactCommand.getFunFactList().contains(funFact));
    }

    @Test
    public void testGetDetail(){
        assertEquals("Command yang akan memberikan sebuah fun fact :D.\n" +
                "How to use:\n/funFact",
                funFactCommand.getDetail());
    }

    String getTextFromTextMessage(Message message){
        TextMessage textMessage = (TextMessage) message;
        return textMessage.getText();
    }
}
