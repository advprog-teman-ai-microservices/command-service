package ai.teman.commandservice.core.command.basic.help;

import ai.teman.commandservice.core.command.basic.help.flex_message.DailyMotivationHelpFlexMessage;
import com.linecorp.bot.model.message.FlexMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DailyMotivationHelpCommandTest {
    private DailyMotivationHelpCommand dailyMotivationHelpCommand;

    @BeforeEach
    public void setUp() {
        dailyMotivationHelpCommand = new DailyMotivationHelpCommand();
    }

    @Test
    public void testGetName() {
        assertEquals("/dailyMotivationHelp", dailyMotivationHelpCommand.getName());
    }

    @Test
    public void testGetDetail() {
        assertEquals("Command untuk menampilkan bantuan daily motivation dalam menggunakan Bot Temanai.\n"
                        + "How to use:\n/dailyMotivationHelp",
                dailyMotivationHelpCommand.getDetail());
    }

    @Test
    public void testProcessCommand(){
        FlexMessage dailyMotivationHelpProcessCommand = dailyMotivationHelpCommand.processCommand(
                dailyMotivationHelpCommand.getName());
        DailyMotivationHelpFlexMessage dailyMotivationHelpFlexMessage = new DailyMotivationHelpFlexMessage();

        assertTrue(dailyMotivationHelpProcessCommand.equals(dailyMotivationHelpFlexMessage.get()));
    }
}
