package ai.teman.commandservice.core.command.basic.help;

import ai.teman.commandservice.core.command.basic.help.flex_message.RegisterAccountHelpFlexMessage;
import com.linecorp.bot.model.message.FlexMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RegisterAccountHelpCommandTest {
    private RegisterAccountHelpCommand registerAccountHelpCommand;

    @BeforeEach
    public void setUp() {
        registerAccountHelpCommand = new RegisterAccountHelpCommand();
    }

    @Test
    public void testGetName() {
        assertEquals("/registerAccountHelp", registerAccountHelpCommand.getName());
    }

    @Test
    public void testGetDetail() {
        assertEquals("Command untuk menampilkan bantuan register account dalam menggunakan Bot Temanai.\n"
                        + "How to use:\n/registerAccountHelp",
                registerAccountHelpCommand.getDetail());
    }

    @Test
    public void testProcessCommand(){
        FlexMessage registerAccountHelpProcessCommand = registerAccountHelpCommand.processCommand(
                registerAccountHelpCommand.getName());
        RegisterAccountHelpFlexMessage registerAccountHelpFlexMessage = new RegisterAccountHelpFlexMessage();

        assertTrue(registerAccountHelpProcessCommand.equals(registerAccountHelpFlexMessage.get()));
    }
}
