package ai.teman.commandservice.core.command.chatsource;

import ai.teman.commandservice.core.trello.TrelloApi;
import ai.teman.commandservice.core.trello.TrelloBoard;
import ai.teman.commandservice.repository.BoardRoomRepo;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ConnectBoardChatSourceCommandTest {

    @InjectMocks
    private ConnectBoardChatSourceCommand connectBoard;

    @Mock
    private TrelloApi trelloApi;

    @Mock
    private BoardRoomRepo boardRoomRepo;

    @Mock
    Source chatSource;

    @BeforeEach
    void setUp() {
        BoardRoomRepo boardRoom = mock(BoardRoomRepo.class);
        connectBoard = new ConnectBoardChatSourceCommand(boardRoom);
    }

    //Setup method to mock Singleton class
    //Reference: https://stackoverflow.com/questions/38914433/mocking-a-singleton-with-mockito
    private void setMock(TrelloApi mock) {
        try {
            Field instance = TrelloApi.class.getDeclaredField("instance");
            instance.setAccessible(true);
            instance.set(instance, mock);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void testGetName() {
        assertEquals("/connectBoard", connectBoard.getName());
    }

    @Test
    void testGetDetail() {
        assertNotNull(connectBoard.getDetail());
    }

    @Test
    void testProcessCommandFail() {
        String result = getTextFromTextMessage(connectBoard.processCommand("test", null));
        assertTrue(result != null && !result.equals(""));
    }

    @Test
    void testProcessCommandSuccess() {
        setMock(trelloApi);
        chatSource = mock(Source.class);
        boardRoomRepo = mock(BoardRoomRepo.class);
        TrelloBoard boardInfo = new TrelloBoard();
        boardInfo.id = "contoh123456";
        boardInfo.name = "Contoh 123";

        when(trelloApi.getBoardInfo("contoh123")).thenReturn(boardInfo);
        String result = getAtlTextFromFlexMessage(connectBoard.processCommand(
                "/connectBoard https://trello.com/b/contoh123/test-temanai-board",
                chatSource));
        assertEquals("Board Contoh 123 is successfully connected", result);
    }

    String getAtlTextFromFlexMessage(Message message){
        FlexMessage flexMessage = (FlexMessage) message;
        return flexMessage.getAltText();
    }

    String getTextFromTextMessage(Message message){
        TextMessage textMessage = (TextMessage) message;
        return textMessage.getText();
    }

}
