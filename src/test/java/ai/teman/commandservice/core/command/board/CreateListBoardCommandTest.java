package ai.teman.commandservice.core.command.board;

import ai.teman.commandservice.core.trello.TrelloApi;
import ai.teman.commandservice.core.trello.TrelloList;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.message.FlexMessage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CreateListBoardCommandTest {
    @Mock
    private TrelloApi trelloApi;

    @InjectMocks
    private CreateListBoardCommand createListCommand;

    @Test
    public void testGetName() {
        assertEquals("/createList", createListCommand.getName());
    }

    @Test
    public void testGetDetail() {
        assertEquals("Command untuk membuat sebuah list di Trello.\n" +
                "How to use:\n/createList\nnamaList", createListCommand.getDetail());
    }

    //Setup method to mock Singleton class
    //Reference: https://stackoverflow.com/questions/38914433/mocking-a-singleton-with-mockito
    private void setMock(TrelloApi mock) {
        try {
            Field instance = TrelloApi.class.getDeclaredField("instance");
            instance.setAccessible(true);
            instance.set(instance, mock);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void testProcessCommand() {
        //Init mocks
        setMock(trelloApi);
        TrelloList trelloList = mock(TrelloList.class);
        trelloList.name = "ToDoList";
        trelloList.id = "listTest";

        Source source = mock(Source.class);

        //Test invalid argument
        assertEquals("Invalid argument for /createList. See /help for details",
                getTextFromTextMessage(createListCommand.
                        processCommand("/createList", source, "boardTest")));

        //Test success create
        assertEquals("List 'ToDoList' is successfully created",
                getAtlTextFromFlexMessage(
                        createListCommand.processCommand("/createList\nToDoList", source, "boardTest")));
        verify(trelloApi).createList(any(TrelloList.class));
    }

    String getTextFromTextMessage(Message message){
        TextMessage textMessage = (TextMessage) message;
        return textMessage.getText();
    }

    String getAtlTextFromFlexMessage(Message message){
        FlexMessage flexMessage = (FlexMessage) message;
        return flexMessage.getAltText();
    }

}
