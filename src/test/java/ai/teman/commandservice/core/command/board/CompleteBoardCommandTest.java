package ai.teman.commandservice.core.command.board;

import ai.teman.commandservice.core.trello.CardNotFoundException;
import ai.teman.commandservice.core.trello.ListNotFoundException;
import ai.teman.commandservice.core.trello.TrelloApi;
import ai.teman.commandservice.core.trello.TrelloCard;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CompleteBoardCommandTest {

    @InjectMocks
    private CompleteBoardCommand completeCommand;

    @Mock
    private TrelloApi trelloApi;

    @BeforeEach
    public void setUp() {
        completeCommand = new CompleteBoardCommand();
    }

    //Setup method to mock Singleton class
    //Reference: https://stackoverflow.com/questions/38914433/mocking-a-singleton-with-mockito
    private void setMock(TrelloApi mock) {
        try {
            Field instance = TrelloApi.class.getDeclaredField("instance");
            instance.setAccessible(true);
            instance.set(instance, mock);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void testGetName() {
        assertEquals("/complete", completeCommand.getName());
    }

    @Test
    public void testGetDetail() {
        assertEquals("Command untuk mengubah status task pada card yang memiliki tenggat waktu menjadi 'complete'.\n" +
                        "How to use:\n/complete\nListName\nCardName",
                completeCommand.getDetail());
    }

    @Test
    public void testProcessCommandFail(){
        String result = getTextFromTextMessage(
                completeCommand.processCommand(completeCommand.getName(), null, ""));
        assertTrue(result != null && result != "");
    }

    @Test
    void testProcessCommandSuccess() {
        setMock(trelloApi);
        TrelloCard card = mock(TrelloCard.class);
        card.name = "B";
        card.id = "123";

        try {
            when(trelloApi.getCard("board1", "A", "B"))
                    .thenReturn(card);
        } catch (Exception ignored) {}
        doNothing().when(trelloApi).updateCard("123", card);

        Source source = mock(Source.class);

        String result = getAtlTextFromFlexMessage(completeCommand.processCommand("/complete\nA\nB",
                source, "board1"));
        assertEquals("Card B in A is successfully completed", result);

        try {
            when(trelloApi.getCard("board1", "A", "B"))
                    .thenReturn(card);
        } catch (Exception ignored) {}

        result = getTextFromTextMessage(completeCommand.processCommand("/complete\nA\nB",
                source, "board1"));
        assertEquals("Card B in A is already completed before", result);

        try {
            when(trelloApi.getCard("board2", "A", "B"))
                    .thenThrow(new ListNotFoundException());
        } catch (Exception ignored) {}

        result = getTextFromTextMessage(completeCommand.processCommand("/complete\nA\nB",
                source, "board2"));
        assertEquals("List A is not found", result);

        try {
            when(trelloApi.getCard("board3", "A", "B"))
                    .thenThrow(new CardNotFoundException());
        } catch (Exception ignored) {}

        result = getTextFromTextMessage(completeCommand.processCommand("/archiveCard\nA\nB",
                source, "board3"));
        assertEquals("Card B is not found", result);
    }

    String getAtlTextFromFlexMessage(Message message){
        FlexMessage flexMessage = (FlexMessage) message;
        return flexMessage.getAltText();
    }

    String getTextFromTextMessage(Message message){
        TextMessage textMessage = (TextMessage) message;
        return textMessage.getText();
    }
}


