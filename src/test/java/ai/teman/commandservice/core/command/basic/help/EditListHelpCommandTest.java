package ai.teman.commandservice.core.command.basic.help;

import ai.teman.commandservice.core.command.basic.help.flex_message.EditListHelpFlexMessage;
import com.linecorp.bot.model.message.FlexMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EditListHelpCommandTest {
    private EditListHelpCommand editListHelpCommand;

    @BeforeEach
    public void setUp() {
        editListHelpCommand = new EditListHelpCommand();
    }

    @Test
    public void testGetName() {
        assertEquals("/editListHelp", editListHelpCommand.getName());
    }

    @Test
    public void testGetDetail() {
        assertEquals("Command untuk menampilkan bantuan edit list dalam menggunakan Bot Temanai.\n"
                        + "How to use:\n/editListHelp",
                editListHelpCommand.getDetail());
    }

    @Test
    public void testProcessCommand(){
        FlexMessage editListHelpProcessCommand = editListHelpCommand.processCommand(
                editListHelpCommand.getName());
        EditListHelpFlexMessage editListHelpFlexMessage = new EditListHelpFlexMessage();

        assertTrue(editListHelpProcessCommand.equals(editListHelpFlexMessage.get()));
    }
}
