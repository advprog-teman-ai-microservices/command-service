package ai.teman.commandservice.core.command.board;

import ai.teman.commandservice.core.trello.TrelloApi;
import ai.teman.commandservice.core.trello.TrelloCard;
import ai.teman.commandservice.core.trello.TrelloList;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CreateCardBoardCommandTest {
    @Mock
    private TrelloApi trelloApi;

    @InjectMocks
    private CreateCardBoardCommand createCardCommand;

    @Test
    void testGetName() {
        assertEquals("/createCard", createCardCommand.getName());
    }

    @Test
    public void testGetDetail(){
        assertEquals("Command untuk membuat sebuah card di suatu list pada Trello.\n" +
                "How to use:\n/createCard\nnamaList\nnamaCard\n(optional) tanggalDue" +
                "tanggalDue harus berformat 'yyyy-MM-dd HH:mm'.\n" +
                "Contoh: 2020-12-31 23:59", createCardCommand.getDetail());
    }

    //Setup method to mock Singleton class
    //Reference: https://stackoverflow.com/questions/38914433/mocking-a-singleton-with-mockito
    private void setMock(TrelloApi mock){
        try{
            Field instance = TrelloApi.class.getDeclaredField("instance");
            instance.setAccessible(true);
            instance.set(instance, mock);
        } catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    @Test
    public void testProcessCommand(){
        //Init mocks
        setMock(trelloApi);
        TrelloList trelloList = mock(TrelloList.class);
        trelloList.name = "list";
        trelloList.id = "listtidtest";

        Source source = mock(Source.class);

        when(trelloApi.getBoardLists("boardtest")).thenReturn(new TrelloList[]{trelloList});

        //Test invalid argument
        assertEquals("Invalid argument for /createCard. See /help for details.",
                getTextFromTextMessage(createCardCommand.
                        processCommand("/createCard", source, "boardtest")));

        assertEquals("Invalid argument for /createCard. See /help for details.",
                getTextFromTextMessage(createCardCommand.
                        processCommand("/createCard\na\nb\nc\nd", source, "boardtest")));

        //Test wrong date
        assertEquals(String.format("'23:59 12/31/2000' is invalid date format. Please use: 'yyyy-MM-dd HH:mm'.%n" +
                        "Example: 2020-12-31 23:59."),
                getTextFromTextMessage(createCardCommand.
                        processCommand("/createCard\na\nb\n23:59 12/31/2000",
                        source, "boardtest")));

        //Test success create
        assertEquals("Card 'test' is successfully created on list 'list' !",
                getAtlTextFromFlexMessage(createCardCommand.
                        processCommand("/createCard\nlist\ntest", source, "boardtest")));

        verify(trelloApi).createCard(any(TrelloCard.class));

        //Test success create with valid date
        assertEquals("Card 'test2' is successfully created on list 'list' !",
                getAtlTextFromFlexMessage(createCardCommand.
                        processCommand("/createCard\nlist\ntest2\n2020-12-31 23:59.", source, "boardtest")));

        verify(trelloApi, times(2)).createCard(any(TrelloCard.class));

        //Test list not found
        assertEquals("List 'listfalse' is not found on the connected board.",
                getTextFromTextMessage(createCardCommand.
                        processCommand("/createCard\nlistfalse\ntest", source, "boardtest")));
    }

    String getTextFromTextMessage(Message message){
        TextMessage textMessage = (TextMessage) message;
        return textMessage.getText();
    }

    String getAtlTextFromFlexMessage(Message message){
        FlexMessage flexMessage = (FlexMessage) message;
        return flexMessage.getAltText();
    }
}