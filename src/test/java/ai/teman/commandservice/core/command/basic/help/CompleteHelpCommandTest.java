package ai.teman.commandservice.core.command.basic.help;

import ai.teman.commandservice.core.command.basic.help.flex_message.CompleteHelpFlexMessage;
import com.linecorp.bot.model.message.FlexMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CompleteHelpCommandTest {
    private CompleteHelpCommand completeHelpCommand;

    @BeforeEach
    public void setUp() {
        completeHelpCommand = new CompleteHelpCommand();
    }

    @Test
    public void testGetName() {
        assertEquals("/completeHelp", completeHelpCommand.getName());
    }

    @Test
    public void testGetDetail() {
        assertEquals("Command untuk menampilkan bantuan complete dalam menggunakan Bot Temanai.\n"
                        + "How to use:\n/completeHelp",
                completeHelpCommand.getDetail());
    }

    @Test
    public void testProcessCommand(){
        FlexMessage completeHelpProcessCommand = completeHelpCommand.processCommand(completeHelpCommand.getName());
        CompleteHelpFlexMessage completeHelpFlexMessage = new CompleteHelpFlexMessage();

        assertTrue(completeHelpProcessCommand.equals(completeHelpFlexMessage.get()));
    }
}
