package ai.teman.commandservice.core.command.basic.help;

import ai.teman.commandservice.core.command.basic.help.flex_message.ConnectBoardHelpFlexMessage;
import com.linecorp.bot.model.message.FlexMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ConnectBoardHelpCommandTest {
    private ConnectBoardHelpCommand connectBoardHelpCommand;

    @BeforeEach
    public void setUp() {
        connectBoardHelpCommand = new ConnectBoardHelpCommand();
    }

    @Test
    public void testGetName() {
        assertEquals("/connectBoardHelp", connectBoardHelpCommand.getName());
    }

    @Test
    public void testGetDetail() {
        assertEquals("Command untuk menampilkan bantuan connect board dalam menggunakan Bot Temanai.\n"
                        + "How to use:\n/connectBoardHelp",
                connectBoardHelpCommand.getDetail());
    }

    @Test
    public void testProcessCommand(){
        FlexMessage connectBoardHelpProcessCommand = connectBoardHelpCommand.processCommand(
                connectBoardHelpCommand.getName());
        ConnectBoardHelpFlexMessage connectBoardHelpFlexMessage = new ConnectBoardHelpFlexMessage();

        assertTrue(connectBoardHelpProcessCommand.equals(connectBoardHelpFlexMessage.get()));
    }
}
