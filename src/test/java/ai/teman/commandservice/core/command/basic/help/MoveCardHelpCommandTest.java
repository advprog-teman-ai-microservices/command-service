package ai.teman.commandservice.core.command.basic.help;

import ai.teman.commandservice.core.command.basic.help.flex_message.MoveCardHelpFlexMessage;
import com.linecorp.bot.model.message.FlexMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MoveCardHelpCommandTest {
    private MoveCardHelpCommand moveCardHelpCommand;

    @BeforeEach
    public void setUp() {
        moveCardHelpCommand = new MoveCardHelpCommand();
    }

    @Test
    public void testGetName() {
        assertEquals("/moveCardHelp", moveCardHelpCommand.getName());
    }

    @Test
    public void testGetDetail() {
        assertEquals("Command untuk menampilkan bantuan move card dalam menggunakan Bot Temanai.\n"
                        + "How to use:\n/moveCardHelp",
                moveCardHelpCommand.getDetail());
    }

    @Test
    public void testProcessCommand(){
        FlexMessage moveCardHelpProcessCommand = moveCardHelpCommand.processCommand(
                moveCardHelpCommand.getName());
        MoveCardHelpFlexMessage moveCardHelpFlexMessage = new MoveCardHelpFlexMessage();

        assertTrue(moveCardHelpProcessCommand.equals(moveCardHelpFlexMessage.get()));
    }
}
