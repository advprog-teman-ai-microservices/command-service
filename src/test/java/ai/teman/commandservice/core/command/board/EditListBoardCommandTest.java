package ai.teman.commandservice.core.command.board;

import ai.teman.commandservice.core.trello.*;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class EditListBoardCommandTest {

    @InjectMocks
    private EditListBoardCommand editList;

    @Mock
    private TrelloApi trelloApi;

    @BeforeEach
    public void setUp() {
        editList = new EditListBoardCommand();
    }

    //Setup method to mock Singleton class
    //Reference: https://stackoverflow.com/questions/38914433/mocking-a-singleton-with-mockito
    private void setMock(TrelloApi mock) {
        try {
            Field instance = TrelloApi.class.getDeclaredField("instance");
            instance.setAccessible(true);
            instance.set(instance, mock);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void testGetName() {
        assertEquals("/editList", editList.getName());
    }

    @Test
    public void testGetDetail() {
        assertEquals( "Command untuk mengedit nama dari sebuah list di Trello.\n"
                        + "How to use:\n/editList\nOldName\nNewName",
                editList.getDetail());
    }

    @Test
    public void testProcessCommandFail() {
        String result = getTextFromTextMessage(editList.processCommand(editList.getName(), null, ""));
        assertTrue(result != null && !result.equals(""));
    }

    @Test
    void testProcessCommandSuccess() {
        setMock(trelloApi);
        TrelloList list = mock(TrelloList.class);
        list.name = "A";
        list.id = "123";

        try {
            when(trelloApi.getList("board1", "A"))
                    .thenReturn(list);
        } catch (Exception ignored) {}

        list.name = "B";
        doNothing().when(trelloApi).updateList("123", list);

        Source source = mock(Source.class);

        String result = getAtlTextFromFlexMessage(editList.processCommand("/editList\nA\nB",
                source, "board1"));
        assertEquals("List A is successfully updated", result);

        try {
            when(trelloApi.getList("board2", "A"))
                    .thenThrow(new ListNotFoundException());
        } catch (Exception ignored) {}

        result = getTextFromTextMessage(editList.processCommand("/editList\nA\nB",
                source, "board2"));
        assertEquals("List A is not found", result);
    }

    String getAtlTextFromFlexMessage(Message message){
        FlexMessage flexMessage = (FlexMessage) message;
        return flexMessage.getAltText();
    }

    String getTextFromTextMessage(Message message){
        TextMessage textMessage = (TextMessage) message;
        return textMessage.getText();
    }
}

