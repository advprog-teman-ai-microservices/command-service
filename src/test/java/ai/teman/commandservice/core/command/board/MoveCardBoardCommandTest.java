package ai.teman.commandservice.core.command.board;

import ai.teman.commandservice.core.trello.TrelloApi;
import ai.teman.commandservice.core.trello.TrelloCard;
import ai.teman.commandservice.core.trello.TrelloList;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MoveCardBoardCommandTest {
    @Mock
    private TrelloApi trelloApi;

    @InjectMocks
    private MoveCardBoardCommand moveCardCommand;

    @BeforeEach
    public void setUp() {
        moveCardCommand = new MoveCardBoardCommand();
    }

    @Test
    public void testGetName() {
        assertEquals("/move", moveCardCommand.getName());
    }

    @Test
    public void testGetDetail() {
        assertEquals( "Command untuk memindahkan sebuah card di suatu list ke list lain pada Trello.\n" +
                "How to use:\n/move\namaListLama\nnamaCard\nnamaListBaru\n" +
                "Kedua List harus ada di board, dan namaListLama harus mempunyai card yang ingin dipindah",
                moveCardCommand.getDetail());
    }


    //Setup method to mock Singleton class
    //Reference: https://stackoverflow.com/questions/38914433/mocking-a-singleton-with-mockito
    private void setMock(TrelloApi mock) {
        try {
            Field instance = TrelloApi.class.getDeclaredField("instance");
            instance.setAccessible(true);
            instance.set(instance, mock);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void testProcessCommand() {
        //Init mocks
        setMock(trelloApi);
        TrelloList trelloList1 = mock(TrelloList.class);
        trelloList1.name = "list1";
        trelloList1.id = "listidtest1";

        TrelloList trelloList2 = mock(TrelloList.class);
        trelloList2.name = "list2";
        trelloList2.id = "listidtest2";

        TrelloCard card = mock(TrelloCard.class);
        card.name = "card";
        card.id = "cardid";
        card.idList = "listidtest1";

        Source source = mock(Source.class);

        when(trelloApi.getBoardLists("boardtest")).thenReturn(new TrelloList[]{trelloList1, trelloList2});
        when(trelloApi.getListCards("listidtest1")).thenReturn(new TrelloCard[]{card});

        //Test invalid argument
        assertEquals("Invalid argument for /move. See /help for details.",
                getTextFromTextMessage(moveCardCommand.
                        processCommand("/move\na", source, "boardtest")));

        //Test old list and new list have same name
        assertEquals("Error: Old list and new list have the same name.",
                getTextFromTextMessage(moveCardCommand.
                        processCommand("/move\na\nb\na", source, "boardtest")));

        //Test old list isn't found
        assertEquals("List 'listfalse1' not found on connected board.",
                getTextFromTextMessage(moveCardCommand.
                        processCommand("/move\nlistfalse1\nb\nlist2", source, "boardtest")));

        //Test new list isn't found
        assertEquals("List 'listfalse2' not found on connected board.",
                getTextFromTextMessage(moveCardCommand.
                        processCommand("/move\nlist1\nb\nlistfalse2", source, "boardtest")));

        //Test card not on old list
        assertEquals("Card 'cardfalse' is not found on list 'list1'.",
                getTextFromTextMessage(moveCardCommand.
                        processCommand("/move\nlist1\ncardfalse\nlist2", source, "boardtest")));

        //Test success move to new list
        assertEquals("Card 'card' on list 'list1' is successfully moved to list 'list2'.",
                getAtlTextFromFlexMessage(moveCardCommand.
                        processCommand("/move\nlist1\ncard\nlist2", source, "boardtest")));
    }

    String getTextFromTextMessage(Message message){
        TextMessage textMessage = (TextMessage) message;
        return textMessage.getText();
    }

    String getAtlTextFromFlexMessage(Message message){
        FlexMessage flexMessage = (FlexMessage) message;
        return flexMessage.getAltText();
    }
}
