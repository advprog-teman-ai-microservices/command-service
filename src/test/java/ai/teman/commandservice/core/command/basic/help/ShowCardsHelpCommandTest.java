package ai.teman.commandservice.core.command.basic.help;

import ai.teman.commandservice.core.command.basic.help.flex_message.ShowCardsHelpFlexMessage;
import com.linecorp.bot.model.message.FlexMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ShowCardsHelpCommandTest {
    private ShowCardsHelpCommand showCardsHelpCommand;

    @BeforeEach
    public void setUp() {
        showCardsHelpCommand = new ShowCardsHelpCommand();
    }

    @Test
    public void testGetName() {
        assertEquals("/showCardsHelp", showCardsHelpCommand.getName());
    }

    @Test
    public void testGetDetail() {
        assertEquals("Command untuk menampilkan bantuan show cards dalam menggunakan Bot Temanai.\n"
                        + "How to use:\n/showCardsHelp",
                showCardsHelpCommand.getDetail());
    }

    @Test
    public void testProcessCommand(){
        FlexMessage showCardsHelpProcessCommand = showCardsHelpCommand.processCommand(
                showCardsHelpCommand.getName());
        ShowCardsHelpFlexMessage showCardsHelpFlexMessage = new ShowCardsHelpFlexMessage();

        assertTrue(showCardsHelpProcessCommand.equals(showCardsHelpFlexMessage.get()));
    }
}
