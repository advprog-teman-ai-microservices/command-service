package ai.teman.commandservice.core.command.basic.help;

import ai.teman.commandservice.core.command.basic.help.flex_message.HelpFlexMessage;
import com.linecorp.bot.model.message.FlexMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class HelpBasicCommandTest {
    private HelpBasicCommand helpCommand;

    @BeforeEach
    public void setUp() {
        helpCommand = new HelpBasicCommand();
    }

    @Test
    public void testGetName() {
        assertEquals("/help", helpCommand.getName());
    }

    @Test
    public void testGetDetail() {
        assertEquals("Command untuk menampilkan bantuan dalam menggunakan Bot Temanai.\n" +
                        "How to use:\n/help",
                helpCommand.getDetail());
    }

    @Test
    public void testProcessCommand(){
        FlexMessage helpProcessCommand = helpCommand.processCommand(helpCommand.getName());
        HelpFlexMessage helpFlexMessage = new HelpFlexMessage();

        assertTrue(helpProcessCommand.equals(helpFlexMessage.get()));
    }
}

