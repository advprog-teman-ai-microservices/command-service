package ai.teman.commandservice.core.command.basic.help;

import ai.teman.commandservice.core.command.basic.help.flex_message.NeedHelpFlexMessage;
import com.linecorp.bot.model.message.FlexMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class NeedHelpCommandTest {
    private NeedHelpCommand needHelpCommand;

    @BeforeEach
    public void setUp() {
        needHelpCommand = new NeedHelpCommand();
    }

    @Test
    public void testGetName() {
        assertEquals("/needHelp", needHelpCommand.getName());
    }

    @Test
    public void testGetDetail() {
        assertEquals("Command untuk menampilkan bantuan help dalam menggunakan Bot Temanai.\n"
                        + "How to use:\n/needHelp",
                needHelpCommand.getDetail());
    }

    @Test
    public void testProcessCommand(){
        FlexMessage needHelpProcessCommand = needHelpCommand.processCommand(
                needHelpCommand.getName());
        NeedHelpFlexMessage needHelpFlexMessage = new NeedHelpFlexMessage();

        assertTrue(needHelpProcessCommand.equals(needHelpFlexMessage.get()));
    }
}
