package ai.teman.commandservice.core.command.chatsource;

import com.linecorp.bot.model.event.source.Source;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ChatSourceCommandTest {
    private Class<?> commandClass;

    @BeforeEach
    void setUp() {
        commandClass = ChatSourceCommand.class;
    }

    @Test
    void testCommandIsAPublicInterface() {
        int classModifiers = commandClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    void testCommandHasProcessCommandAbstractMethod() throws Exception {
        Method cast = commandClass.getDeclaredMethod("processCommand", String.class, Source.class);
        int methodModifiers = cast.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(2, cast.getParameterCount());
    }
}
