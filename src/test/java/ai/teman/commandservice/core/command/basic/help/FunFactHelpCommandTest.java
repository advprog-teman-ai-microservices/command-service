package ai.teman.commandservice.core.command.basic.help;

import ai.teman.commandservice.core.command.basic.help.flex_message.FunFactHelpFlexMessage;
import com.linecorp.bot.model.message.FlexMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FunFactHelpCommandTest {
    private FunFactHelpCommand funFactHelpCommand;

    @BeforeEach
    public void setUp() {
        funFactHelpCommand = new FunFactHelpCommand();
    }

    @Test
    public void testGetName() {
        assertEquals("/funFactHelp", funFactHelpCommand.getName());
    }

    @Test
    public void testGetDetail() {
        assertEquals("Command untuk menampilkan bantuan fun fact dalam menggunakan Bot Temanai.\n"
                        + "How to use:\n/funFactHelp",
                funFactHelpCommand.getDetail());
    }

    @Test
    public void testProcessCommand(){
        FlexMessage funFactHelpProcessCommand = funFactHelpCommand.processCommand(
                funFactHelpCommand.getName());
        FunFactHelpFlexMessage funFactHelpFlexMessage = new FunFactHelpFlexMessage();

        assertTrue(funFactHelpProcessCommand.equals(funFactHelpFlexMessage.get()));
    }
}
