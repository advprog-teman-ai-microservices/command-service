package ai.teman.commandservice.core.command.basic.help;

import ai.teman.commandservice.core.command.basic.help.flex_message.EditCardHelpFlexMessage;
import com.linecorp.bot.model.message.FlexMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EditCardHelpCommandTest {
    private EditCardHelpCommand editCardHelpCommand;

    @BeforeEach
    public void setUp() {
        editCardHelpCommand = new EditCardHelpCommand();
    }

    @Test
    public void testGetName() {
        assertEquals("/editCardHelp", editCardHelpCommand.getName());
    }

    @Test
    public void testGetDetail() {
        assertEquals("Command untuk menampilkan bantuan edit card dalam menggunakan Bot Temanai.\n"
                        + "How to use:\n/editCardHelp",
                editCardHelpCommand.getDetail());
    }

    @Test
    public void testProcessCommand(){
        FlexMessage editCardHelpProcessCommand = editCardHelpCommand.processCommand(
                editCardHelpCommand.getName());
        EditCardHelpFlexMessage editCardHelpFlexMessage = new EditCardHelpFlexMessage();

        assertTrue(editCardHelpProcessCommand.equals(editCardHelpFlexMessage.get()));
    }
}
