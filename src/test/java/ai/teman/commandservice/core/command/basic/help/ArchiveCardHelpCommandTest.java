package ai.teman.commandservice.core.command.basic.help;

import ai.teman.commandservice.core.command.basic.help.flex_message.ArchiveCardHelpFlexMessage;
import com.linecorp.bot.model.message.FlexMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ArchiveCardHelpCommandTest {
    private ArchiveCardHelpCommand archiveCardHelpCommand;

    @BeforeEach
    public void setUp() {
        archiveCardHelpCommand = new ArchiveCardHelpCommand();
    }

    @Test
    public void testGetName() {
        assertEquals("/archiveCardHelp", archiveCardHelpCommand.getName());
    }

    @Test
    public void testGetDetail() {
        assertEquals("Command untuk menampilkan bantuan archive card dalam menggunakan Bot Temanai.\n"
                + "How to use:\n/archiveCardHelp",
                archiveCardHelpCommand.getDetail());
    }

    @Test
    public void testProcessCommand(){
        FlexMessage archiveCardHelpProcessCommand = archiveCardHelpCommand.processCommand(archiveCardHelpCommand.getName());
        ArchiveCardHelpFlexMessage archiveCardHelpFlexMessage = new ArchiveCardHelpFlexMessage();

        assertTrue(archiveCardHelpProcessCommand.equals(archiveCardHelpFlexMessage.get()));
    }
}
