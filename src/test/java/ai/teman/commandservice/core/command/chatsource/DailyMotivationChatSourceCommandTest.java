package ai.teman.commandservice.core.command.chatsource;

import ai.teman.commandservice.controller.CommandServiceController;
import ai.teman.commandservice.core.trello.TrelloApi;
import com.linecorp.bot.model.event.source.GroupSource;
import com.linecorp.bot.model.event.source.UnknownSource;
import com.linecorp.bot.model.event.source.UserSource;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.event.source.Source;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class DailyMotivationChatSourceCommandTest {
    private DailyMotivationChatSourceCommand dailyMotivation;
    private CommandServiceController serviceController;
    private static ScheduledExecutorService scheduledService;

    @BeforeEach
    public void setUp() {
        dailyMotivation = DailyMotivationChatSourceCommand.getInstance();
    }

    @Test
    public void testGetName() {
        assertEquals("/setDailyMotivation", dailyMotivation.getName());
    }

    @Test
    public void testGetDetail() {
        assertEquals( "Command untuk memberikan pesan motivasi harian.\n"
                + "How to use:\n/setDailyMotivation " + "jumlahHari\n"
                + "Contoh : /setDailyMotivation 3\n"
                + "Berarti motivasi akan diberikan dalam waktu 3 hari sekali.",
                dailyMotivation.getDetail());
    }

    @Test
    public void testSetterThreadPool() {
        DailyMotivationChatSourceCommand.setThreadPool(1);
        assertNotNull(DailyMotivationChatSourceCommand.getScheduledService());

        DailyMotivationChatSourceCommand.getScheduledService().shutdown();
        assertTrue(DailyMotivationChatSourceCommand.getScheduledService().isShutdown());
    }

    @Test
    public void testGetterSetterScheduledService() {
        DailyMotivationChatSourceCommand.setScheduledService(null);
        assertNull(DailyMotivationChatSourceCommand.getScheduledService());
    }

    @Test
    public void testGetIdSender() {
        Source source = mock(Source.class);
        if (source instanceof UserSource) {
            assertEquals(source.getUserId(), dailyMotivation.getIdSender());
        } else if (source instanceof GroupSource) {
            assertEquals(((GroupSource)source).getGroupId(), dailyMotivation.getIdSender());
        } else if (source instanceof UnknownSource) {
            assertEquals(source.getSenderId(), dailyMotivation.getIdSender());
        } else {
            assertEquals(source.getSenderId(), dailyMotivation.getIdSender());
        }
    }

    @Test
    public void testGetController() {
        dailyMotivation.setController(serviceController);
        assertEquals(serviceController, dailyMotivation.getController());
    }

    String getTextFromTextMessage(Message message){
        TextMessage textMessage = (TextMessage) message;
        return textMessage.getText();
    }
}
