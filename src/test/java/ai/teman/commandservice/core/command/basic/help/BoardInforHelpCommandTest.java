package ai.teman.commandservice.core.command.basic.help;

import ai.teman.commandservice.core.command.basic.help.flex_message.BoardInfoHelpFlexMessage;
import com.linecorp.bot.model.message.FlexMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BoardInforHelpCommandTest {
    private BoardInfoHelpCommand boardInfoHelpCommand;

    @BeforeEach
    public void setUp() {
        boardInfoHelpCommand = new BoardInfoHelpCommand();
    }

    @Test
    public void testGetName() {
        assertEquals("/boardInfoHelp", boardInfoHelpCommand.getName());
    }

    @Test
    public void testGetDetail() {
        assertEquals("Command untuk menampilkan bantuan board info dalam menggunakan Bot Temanai.\n"
                        + "How to use:\n/boardInfoHelp",
                boardInfoHelpCommand.getDetail());
    }

    @Test
    public void testProcessCommand(){
        FlexMessage boardInfoHelpProcessCommand = boardInfoHelpCommand.processCommand(boardInfoHelpCommand.getName());
        BoardInfoHelpFlexMessage boardInfoHelpFlexMessage = new BoardInfoHelpFlexMessage();

        assertTrue(boardInfoHelpProcessCommand.equals(boardInfoHelpFlexMessage.get()));
    }
}
