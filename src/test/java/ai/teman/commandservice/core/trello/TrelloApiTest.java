package ai.teman.commandservice.core.trello;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TrelloApiTest {

    private Class<?> trelloApiClass;

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private TrelloApi trelloApi;

    private final String key = System.getenv("TRELLO_API_KEY");
    private final String token = System.getenv("TRELLO_API_TOKEN");

    @BeforeEach
    public void setUp() throws Exception {
        trelloApiClass = Class.forName(TrelloApi.class.getName());
    }

    private String getSecrets(){
        return "?key=" + key + "&token=" + token;
    }

    private HttpEntity<String> convertObjectToJsonHTTPEntity(Object o) {
        ObjectMapper mapper = new ObjectMapper();

        //for test
        mapper.setAnnotationIntrospector(new JacksonAnnotationIntrospector() {

            @Override
            public boolean hasIgnoreMarker(final AnnotatedMember m) {
                return super.hasIgnoreMarker(m) || m.getName().contains("Mockito");
            }
        });
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        try{
            return new HttpEntity<>(mapper.writeValueAsString(o), headers);
        } catch (JsonProcessingException e){
            return null;
        }
    }

    @Test
    public void testNoPublicConstructors() {
        List<Constructor> constructors = Arrays.asList(trelloApiClass.getDeclaredConstructors());

        boolean check = constructors.stream()
                .anyMatch(c -> !Modifier.isPrivate(c.getModifiers()));

        assertFalse(check);
    }

    @Test
    public void testGetInstanceShouldReturnSingletonInstance() {
        TrelloApi firstInstance = TrelloApi.getInstance();
        TrelloApi secondInstance = TrelloApi.getInstance();
        assertEquals(firstInstance, secondInstance);
    }

    @Test
    public void testWhenGetBoardListIsCalledItShouldCallRestTemplateWithCorrectURL(){
        String boardId = "boardtest";
        trelloApi.getBoardLists(boardId);
        verify(restTemplate).getForObject(
                "https://api.trello.com/1/boards/" + boardId + "/lists" + getSecrets(),
                TrelloList[].class);
    }

    @Test
    public void testWhenGetListCardIsCalledItShouldCallRestTemplateWithCorrectURL(){
        String listId = "listtest";
        trelloApi.getListCards(listId);
        verify(restTemplate).getForObject(
                "https://api.trello.com/1/lists/" + listId + "/cards" + getSecrets(),
                TrelloCard[].class);
    }

    @Test
    public void testWhenUpdateCardIsCalledItShouldCallRestTemplateWithCorrectURL(){
        TrelloCard mockCard = mock(TrelloCard.class);
        mockCard.id = "testid";
        String cardId = mockCard.id;

        trelloApi.updateCard(cardId, mockCard);
        verify(restTemplate).put(
                "https://api.trello.com/1/cards/" + cardId + getSecrets(),
                convertObjectToJsonHTTPEntity(mockCard));
    }

    @Test
    public void testWhenCreateCardIsCalledItShouldCallRestTemplateWithCorrectURL(){
        TrelloCard mockCard = mock(TrelloCard.class);
        mockCard.id = "testid";
        String cardId = mockCard.id;

        trelloApi.createCard(mockCard);
        verify(restTemplate).postForObject(
                "https://api.trello.com/1/cards/" + getSecrets(),
                convertObjectToJsonHTTPEntity(mockCard),
                TrelloCard.class);
    }

    @Test
    public void testWhenCreateListIsCalledItShouldCallRestTemplateWithCorrectURL() {
        TrelloList mockList = mock(TrelloList.class);
        mockList.id = "testid";
        String listId = mockList.id;

        trelloApi.createList(mockList);
        verify(restTemplate).postForObject(
                "https://api.trello.com/1/lists/" + getSecrets(),
                convertObjectToJsonHTTPEntity(mockList),
                TrelloList.class);
    }

     @Test
    void testWhenGetListIsCalledItShouldCallGetBoardLists() {
        try {
            TrelloList trelloList = new TrelloList();
            trelloList.name = "random-list";

            TrelloList[] trelloLists = new TrelloList[]{trelloList};
            when(trelloApi.getBoardLists("a")).thenReturn(trelloLists);

            TrelloList result = trelloApi.getList("a", "random-list");
            assertEquals(trelloList, result);
            trelloApi.getList("a", "doesnt-exist");
        } catch(ListNotFoundException e) {
            assertNotNull(e);
        }
    }

    @Test
    public void testWhenUpdateListIsCalledItShouldCallRestTemplateWithCorrectURL(){
        TrelloList mockList = mock(TrelloList.class);
        mockList.id = "testid";
        String listId = mockList.id;

        trelloApi.updateList(listId, mockList);
        verify(restTemplate).put(
                "https://api.trello.com/1/lists/" + listId + getSecrets(),
                convertObjectToJsonHTTPEntity(mockList));
    }

    @Test
    void testWhenGetCardIsCalledItShouldCallGetListAndGetListCards() {
        try {
            TrelloList trelloList = new TrelloList();
            trelloList.name = "random-list";

            TrelloList[] trelloLists = new TrelloList[]{trelloList};
            when(trelloApi.getBoardLists("a")).thenReturn(trelloLists);

            TrelloCard myCard = new TrelloCard();
            myCard.name = "sleep";

            TrelloCard[] trelloCards = new TrelloCard[]{myCard};
            when(trelloApi.getListCards(trelloList.id)).thenReturn(trelloCards);

            TrelloCard result = trelloApi.getCard("a", "random-list", "sleep");
            assertEquals(myCard, result);
            trelloApi.getCard("a", "b", "c");
        } catch (ListNotFoundException | CardNotFoundException e) {
            assertNotNull(e);
        }
    }
}
