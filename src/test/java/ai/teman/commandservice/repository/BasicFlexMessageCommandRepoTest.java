package ai.teman.commandservice.repository;

import ai.teman.commandservice.core.command.basic.BasicFlexMessageCommand;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class BasicFlexMessageCommandRepoTest {
    public BasicFlexMessageCommandRepo basicFlexMessageCommandRepo;

    @BeforeEach
    public void setUp(){
        basicFlexMessageCommandRepo = new BasicFlexMessageCommandRepo();
    }

    @Test
    public void testProcessCommandShouldReturnResultOfProcessCommandIfCommandExist(){
        BasicFlexMessageCommand mockCommand = mock(BasicFlexMessageCommand.class);
        when(mockCommand.getName()).thenReturn("/mock");
        Message mockResult = new TextMessage("Mocked Result");
        when(mockCommand.processCommand(anyString())).thenReturn(mockResult);
        basicFlexMessageCommandRepo.registerCommand(mockCommand);

        assertEquals(mockResult,
                basicFlexMessageCommandRepo.processCommand("/mock", "/mock"));
    }

    @Test
    public void testProcessCommandShouldReturnNullIfCommandDoesNotExist(){
        assertNull(basicFlexMessageCommandRepo.processCommand("/doesnt-exist", "/null"));
    }
}
