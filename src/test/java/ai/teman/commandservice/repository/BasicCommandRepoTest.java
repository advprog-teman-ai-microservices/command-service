package ai.teman.commandservice.repository;

import ai.teman.commandservice.core.command.basic.BasicCommand;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class BasicCommandRepoTest {
    public BasicCommandRepo basicCommandRepo;

    @BeforeEach
    public void setUp(){
        basicCommandRepo = new BasicCommandRepo();
    }

    @Test
    public void testProcessCommandShouldReturnResultOfProcessCommandIfCommandExist(){
        BasicCommand mockCommand = mock(BasicCommand.class);
        when(mockCommand.getName()).thenReturn("/mock");
        when(mockCommand.processCommand(anyString())).thenReturn(new TextMessage("Mocked Result"));
        basicCommandRepo.registerCommand(mockCommand);

        assertEquals("Mocked Result",
                getTextFromTextMessage(
                        basicCommandRepo.processCommand("/mock", "/mock")));
    }

    @Test
    public void testProcessCommandShouldReturnNullIfCommandDoesNotExist(){
        assertNull(basicCommandRepo.processCommand("/doesnt-exist", "/null"));
    }

    String getTextFromTextMessage(Message message){
        TextMessage textMessage = (TextMessage) message;
        return textMessage.getText();
    }
}
