package ai.teman.commandservice.repository;

import ai.teman.commandservice.core.command.base.BaseCommand;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Map;

import static org.mockito.Mockito.*;

public class BaseCommandRepoTest {
    @Mock
    private Map<String, BaseCommand> mapCommand;

    @InjectMocks
    public BaseCommandRepo baseCommandRepo;

    @BeforeEach
    public void setUp(){
        baseCommandRepo = mock(BaseCommandRepo.class, Answers.CALLS_REAL_METHODS);
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testWhenGetCommandsIsCalledItShouldCallMapCommandValues(){
        baseCommandRepo.getCommands();
        verify(mapCommand).values();
    }

    @Test
    public void testWhenRegisterCommandIsCalledItShouldCallMapCommandPut(){
        BaseCommand commandMock = mock(BaseCommand.class);
        when(commandMock.getName()).thenReturn("commandtest");

        baseCommandRepo.registerCommand(commandMock);
        verify(mapCommand).put("commandtest", commandMock);
    }
}
